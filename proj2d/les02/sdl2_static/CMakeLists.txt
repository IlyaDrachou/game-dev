cmake_minimum_required (VERSION 3.13)
project (sdl2V_static)

add_executable (sdl2V_static main.cxx)
target_compile_features(sdl2V_static PUBLIC cxx_std_17)

#find sdl2 package
find_package(sdl2 REQUIRED)
#try to find local SDL2 headers path
find_path(sdl2_include_local
    SDL.h
    PATHS /usr/local/include/SDL2/)
if (sdl2_include_local)
    #if found local sdl headers
    target_include_directories(sdl2V_static PRIVATE ${sdl2_include_local})
else()
    #else use system sdl headers
    target_include_directories(sdl2V_static PRIVATE ${SDL2_INCLUDE_DIRS})
endif()
#search for sdl static lib (for unix)
find_library(SDL2_LIB libSDL2.a)

if (UNIX AND NOT SDL2_LIB)
    message(FATAL_ERROR "Error: find_library(...) did not find libSDL2.a")
elseif (UNIX AND SDL2_LIB)
    message(STATUS "path to static libSDL2.a [${SDL2_LIB}]")
endif()

if (MINGW)
    message(STATUS "[${CMAKE_CXX_COMPILER_ID}]")
    # find out what libraries are needed for staticaly linking with libSDL.a
    # using mingw64 cross-compiler

    #$> $ /usr/x86_64-w64-mingw32/sys-root/mingw/bin/sdl2-config --static-libs
    #-L/usr/x86_64-w64-mingw32/sys-root/mingw/lib -lmingw32 -lSDL2main
    #-lSDL2 -mwindows -Wl,--no-undefined -lm -ldinput8 -ldxguid -ldxerr8 -luser32
    #-lgdi32 -lwinmm -limm32 -lole32 -loleaut32 -lshell32 -lversion -luuid
    #-static-libgcc
    target_link_libraries(sdl2V_static
           #SDL2::SDL2-static
           #SDL2::SDL2main
           -lSDL2main
           ${SDL2_LIB} # full path to libSDL2.a force to staticaly link with it
           -L/usr/local/lib
           -Wl,-rpath,/usr/local/lib
           -Wl,--enable-new-dtags
           -lglib-2.0
           -lgobject-2.0
           -lgio-2.0
           -libus-1.0
           -ldbus-1
           /usr/lib/x86_64-linux-gnu/libsndio.so
           -ldl
           -lm
           -Wl,--no-undefined
           -pthread
           )
elseif(UNIX)
    if(APPLE)
      # find out what libraries are needed for staticcaly linking with libSDL.a on MacOS with HOMEBREW (currently clang++ not supported c++17)
      # $> /usr/local/bin/sdl2-config --static-libs
      # -L/usr/local/lib -lSDL2 -lm -liconv -Wl,-framework,CoreAudio
      # -Wl,-framework,AudioToolbox -Wl,-framework,ForceFeedback -lobjc -Wl,-framework,CoreVideo -Wl,-framework,Cocoa
      # -Wl,-framework,Carbon -Wl,-framework,IOKit

      target_link_libraries(sdl2V_static
               "${SDL2_LIB}" # full path to libSDL2.a force to staticaly link with it
               -lm
               -liconv
               -Wl,-framework,CoreAudio
               -Wl,-framework,AudioToolbox
               -Wl,-framework,ForceFeedback
               -lobjc
               -Wl,-framework,CoreVideo
               -Wl,-framework,Cocoa
               -Wl,-framework,Carbon
               -Wl,-framework,IOKit
               -Wl,-weak_framework,QuartzCore
               -Wl,-weak_framework,Metal
               )
    else()
      # find out what libraries are needed for staticaly linking with libSDL.a on Linux
      # using default linux compiler
      # $> sdl2-config --static-libs
      # -lSDL2 -Wl,--no-undefined -lm -ldl -lpthread -lrt

      message(STATUS "[${CMAKE_CXX_COMPILER_ID}]")
  # For gitlab-ci
      target_link_libraries(sdl2V_static
          ${SDL2_LIB}
          -lSDL2main
          -Wl,--no-undefined
          -lm
          -ldl
          -lasound
          -lm
          -ldl
          -lpthread
          -lpulse-simple
          -lpulse
          -lsndio
          -lX11
          -lXext
          -lXcursor
          -lXinerama
          -lXi
          -lXrandr
          -lXss
          -lXxf86vm
          -lwayland-egl
          -lwayland-client
          -lwayland-cursor
          -lxkbcommon
          -lpthread
          -lrt)

#      target_link_libraries(sdl2V_static
#             #SDL2::SDL2-static
#             #SDL2::SDL2main
#             -lSDL2main
#             ${SDL2_LIB} # full path to libSDL2.a force to staticaly link with it
#             -L/usr/local/lib
#             -L/usr/lib/x86_64-linux-gnu/
#             -Wl,-rpath,/usr/local/lib
#             -Wl,-rpath,/usr/lib/x86_64-linux-gnu/
#             -Wl,--enable-new-dtags
#             -lglib-2.0
#             -lgobject-2.0
#             -lgio-2.0
#             -libus-1.0
#             -ldbus-1
#             /usr/lib/x86_64-linux-gnu/libsndio.so
#             -lsndio
#             -ldl
#             -lm
#             -Wl,--no-undefined
#             -pthread
#             )
    endif()
elseif (MSVC)
    target_link_libraries(sdl2V_static LINK_PUBLIC SDL2::SDL2main SDL2::SDL2-static c:/tools/vcpkg/installed/x64-windows-static/lib/SDL2.lib)
endif()
