#include <cstdlib>
#include <iostream>

#include <SDL_version.h>

std::ostream& operator<<(std::ostream& out, SDL_version& version)
{
    out << static_cast<int>(version.major) << '.'
        << static_cast<int>(version.minor) << '.'
        << static_cast<int>(version.patch);

    return out;
}

int main(int, char**)
{
    using namespace std;

    SDL_version compiled{};
    SDL_version linked{};

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    cout << compiled << endl;
    cout << linked << endl;

    return (cout.good() ? EXIT_SUCCESS : EXIT_FAILURE);
}
