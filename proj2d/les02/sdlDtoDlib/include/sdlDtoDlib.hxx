#pragma once

#ifdef WIN32
#ifdef stdDtoDlib_EXPORTS
#define DECLSPEC __declspec(dllexport)
#else
#ifndef DECLSPEC
#define DECLSPEC
#else
#define DECLSPEC __declspec(dllimport)
#endif
#endif // stdDtoDlib_EXPORTS
#else
#define DECLSPEC
#endif // WIN32

namespace sdlDtoDlib
{
bool DECLSPEC printSDLVersions();
} // namespace sdlDtoDlib
