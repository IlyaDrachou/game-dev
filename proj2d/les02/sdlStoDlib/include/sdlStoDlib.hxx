#pragma once

#ifdef WIN32
#ifdef stdDtoDlib_EXPORTS
#define DECLSPEC __declspec(dllexport)
#else
#define DECLSPEC __declspec(dllimport)
#endif // stdDtoDlib_EXPORTS
#else
#define DECLSPEC
#endif // WIN32

namespace sdlStoDlib
{
bool DECLSPEC printSDLVersions();
} // namespace sdlStoDlib
