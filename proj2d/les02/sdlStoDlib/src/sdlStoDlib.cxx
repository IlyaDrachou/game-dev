#include "sdlStoDlib.hxx"

#include <SDL_version.h>
#include <iostream>

#undef DECLSPEC

#ifdef WIN32
#define DECLSPEC __declspec(dllexport)
#else
#define DECLSPEC
#endif // WIN32

namespace sdlStoDlib
{
inline struct Versions
{
    SDL_version compiled{};
    SDL_version linked{};
    Versions()
    {
        SDL_VERSION(&linked)
        SDL_GetVersion(&compiled);
    }
} versions;

inline bool checkVersionsTheSame();

inline bool operator==(const SDL_version& first, const SDL_version& second)
{
    return (first.major == second.major && first.minor == second.minor &&
            first.patch == second.patch);
}
std::ostream& operator<<(std::ostream& out, const SDL_version& version)
{
    out << static_cast<int>(version.major) << '.'
        << static_cast<int>(version.minor) << '.'
        << static_cast<int>(version.patch);

    return out;
}

bool DECLSPEC printSDLVersions()
{
    std::cout << "Version of linked (Static sdl to dynamic lib): "
              << versions.linked << std::endl;
    std::cout << "Version of compiled (Static sdl to dynamic lib): "
              << versions.compiled << std::endl;
    if (!checkVersionsTheSame())
    {
        throw std::runtime_error("Versions should be the same!!!");
    }
    return std::cout.good();
}

bool checkVersionsTheSame()
{
    return versions.linked == versions.compiled;
}
} // namespace sdlStoDlib
