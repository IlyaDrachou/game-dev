#include "engine.hxx"
#include <cstdlib>
#include <iostream>
#include <memory>

int main(int, char**)
{
    using namespace std;
    unique_ptr<engn::engine, void (*)(engn::engine*)> engine(
        engn::create_engine(), engn::destroy_engine);

    try
    {
        engine->initialize();

        bool continue_loop{ true };
        while (continue_loop)
        {
            engn::event_reflection_pair event_pair;
            while (engine->read_input(event_pair))
            {
                cout << event_pair.second << endl;
                if (event_pair.first == engn::event::QUIT)
                {
                    continue_loop = false;
                }
            }
        }

        engine->uinitialize();
    }
    catch (runtime_error& error)
    {
        cout << error.what() << endl;
        return EXIT_FAILURE;
    }
    catch (out_of_range& error)
    {
        cout << error.what() << endl;
        cout << "Hint: check that all actions created in handled keys are "
                "exist (and action_keys too)!"
             << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
