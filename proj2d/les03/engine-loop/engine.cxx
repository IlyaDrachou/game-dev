#include "engine.hxx"
#include <SDL.h>
#include <SDL_keycode.h>
#include <SDL_version.h>
#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string_view>
#include <unordered_map>

namespace engn
{
using namespace std;

// keys for event searching should be only UPPERCASE, construct: "eventName" +
// + '_' + "eventType" (ex. AHEAD_PRESSED). For QUIT only eventType.
static std::unordered_map<string, event_reflection_pair> actions{
    { "AHEAD_PRESSED", { event::AHEAD_PRESSED, "Ahead_pressed" } },
    { "AHEAD_RELEASED", { event::AHEAD_RELEASED, "Ahead_released" } },
    { "BACK_PRESSED", { event::BACK_PRESSED, "Back_pressed" } },
    { "BACK_RELEASED", { event::BACK_RELEASED, "Back_released" } },
    { "LEFT_PRESSED", { event::LEFT_PRESSED, "Left_pressed" } },
    { "LEFT_RELEASED", { event::LEFT_RELEASED, "Left_released" } },
    { "RIGHT_PRESSED", { event::RIGHT_PRESSED, "Right_pressed" } },
    { "RIGHT_RELEASED", { event::RIGHT_RELEASED, "Right_released" } },
    { "QUIT", { event::QUIT, "Quit" } }
};

// struct for keys handling
#pragma pack(push, 4)
struct bind
{
    bind(SDL_Keycode key, std::string_view action)
        : m_key(key)
        , m_action(action)
    {
    }
    SDL_Keycode      m_key;
    std::string_view m_action;
};
#pragma pack(pop)

static array<bind, 8> handled_keys{ { { SDLK_w, "AHEAD" },
                                      { SDLK_UP, "AHEAD" },
                                      { SDLK_s, "BACK" },
                                      { SDLK_DOWN, "BACK" },
                                      { SDLK_a, "LEFT" },
                                      { SDLK_LEFT, "LEFT" },
                                      { SDLK_d, "RIGHT" },
                                      { SDLK_RIGHT, "RIGHT" } } };

static bool check_input(std::string_view   event_type,
                        const SDL_Keycode& event_key, event_reflection_pair& e)
{
    std::string postfix{ event_type };
    std::transform(begin(postfix), end(postfix), begin(postfix), ::toupper);

    auto prefix = find_if(std::begin(handled_keys), std::end(handled_keys),
                          [&](const bind& b) { return b.m_key == event_key; });

    if (prefix == end(handled_keys))
    {
        return false;
    }

    e = actions.at(string(prefix->m_action) + '_' + postfix);

    return true;
}

class engine_impl final : public engine
{
    engine_impl() {}

public:
    friend engine* create_engine(); // For anti-pattern singlton
    void           initialize() override final
    {
        SDL_version linked{};
        SDL_GetVersion(&linked);

        if (SDL_COMPILEDVERSION !=
            SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
        {
            throw runtime_error(
                "Versions (compiled and linked) should be the same!!!");
        }

        auto init_result = SDL_Init(SDL_INIT_EVERYTHING);
        if (init_result != 0)
        {
            string what{ "Can't init SDL: " };
            throw runtime_error(what + SDL_GetError());
        }

        auto window =
            SDL_CreateWindow("First SDL window",      // window title
                             SDL_WINDOWPOS_UNDEFINED, // initial x position
                             SDL_WINDOWPOS_UNDEFINED, // initial y position
                             640,                     // width, in pixels
                             480,                     // height, in pixels
                             SDL_WINDOW_OPENGL        // flags - see below
            );

        if (window == nullptr)
        {
            string what{ "Can't create window: " };
            throw runtime_error(what + SDL_GetError());
        }
    }
    bool read_input(event_reflection_pair& e) override final
    {
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event))
        {
            switch (sdl_event.type)
            {
                case SDL_QUIT:
                    e = actions.at("QUIT");
                    return true;
                case SDL_KEYDOWN:
                    return check_input("PRESSED", sdl_event.key.keysym.sym, e);
                case SDL_KEYUP:
                    return check_input("RELEASED", sdl_event.key.keysym.sym, e);
            }
        }
        return false;
    }
    void uinitialize() override final { SDL_Quit(); }
};
engine::~engine() {}

static engine* e = nullptr;
engine*        create_engine()
{
    if (e != nullptr)
    {
        throw runtime_error("Engine already exists!!!");
    }
    e = new engine_impl;
    return e;
}

void destroy_engine(engine* e)
{
    if (e == nullptr)
    {
        throw runtime_error("Engine already destroyed!!!");
    }
    delete e;
}

} // namespace engn
