#pragma once
#include <string_view>
#include <unordered_map>

namespace engn
{
enum class event
{
    AHEAD_PRESSED,
    AHEAD_RELEASED,
    BACK_PRESSED,
    BACK_RELEASED,
    LEFT_PRESSED,
    LEFT_RELEASED,
    RIGHT_PRESSED,
    RIGHT_RELEASED,
    QUIT
};

typedef std::pair<engn::event, std::string_view> event_reflection_pair;

class engine
{
public:
    virtual ~engine();
    virtual void initialize()                         = 0;
    virtual bool read_input(event_reflection_pair& e) = 0;
    virtual void uinitialize()                        = 0;
};

engine* create_engine();
void    destroy_engine(engine*);
} // namespace engn
