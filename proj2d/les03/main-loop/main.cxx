#include <SDL.h>
#include <SDL_keycode.h>
#include <SDL_version.h>
#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <string_view>

static std::map<std::string_view, std::string_view> actions{
    { "AHEAD_PRESSED", "Ahead_pressed" },
    { "AHEAD_RELEASED", "Ahead_released" },
    { "BACK_PRESSED", "Back_pressed" },
    { "BACK_RELEASED", "Back_released" },
    { "LEFT_PRESSED", "Left_pressed" },
    { "LEFT_RELEASED", "Left_released" },
    { "RIGHT_PRESSED", "Right_pressed" },
    { "RIGHT_RELEASED", "Right_released" }
};

#pragma pack(push, 4)
struct bind
{
    bind(SDL_Keycode key, std::string_view action)
        : m_key(key)
        , m_action(action)
    {
    }
    SDL_Keycode      m_key;
    std::string_view m_action;
};
#pragma pack(pop)

std::ostream& operator<<(std::ostream& out, const SDL_version& version)
{
    out << static_cast<int>(version.major) << '.'
        << static_cast<int>(version.minor) << '.'
        << static_cast<int>(version.patch);

    return out;
}

bool check_input(const SDL_Event& event)
{
    const std::array<::bind, 4> keys{ { { SDLK_w, "AHEAD" },
                                        { SDLK_s, "BACK" },
                                        { SDLK_a, "LEFT" },
                                        { SDLK_d, "RIGHT" } } };
    std::string                 postfix;
    std::string		            action;
    if (event.type == SDL_KEYDOWN)
    {
        postfix = "PRESSED";
    }
    else if (event.type == SDL_KEYUP)
    {
        postfix = "RELEASED";
    }
    auto it =
        std::find_if(std::begin(keys), std::end(keys), [&](const ::bind& b) {
            return b.m_key == event.key.keysym.sym;
        });

    if (it != end(keys))
    {
        action = std::string(it->m_action) + '_' + postfix;
        std::cout << actions[action] << std::endl;
    }

    return true;
}

int main(int, char**)
{
    using namespace std;

    SDL_version linked{};
    SDL_GetVersion(&linked);

    cout << (SDL_COMPILEDVERSION ==
             SDL_VERSIONNUM(linked.major, linked.minor, linked.patch));

    auto init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        cout << "Can't init SDL: " << SDL_GetError();
        return EXIT_FAILURE;
    }

    auto window =
        SDL_CreateWindow("First SDL window",      // window title
                         SDL_WINDOWPOS_UNDEFINED, // initial x position
                         SDL_WINDOWPOS_UNDEFINED, // initial y position
                         640,                     // width, in pixels
                         480,                     // height, in pixels
                         SDL_WINDOW_OPENGL        // flags - see below
        );

    if (window == nullptr)
    {
        cout << "Can't create window: " << SDL_GetError();
        return EXIT_FAILURE;
    }

    bool continue_loop{ true };
    while (continue_loop)
    {
        SDL_Event sdl_event;
        while (SDL_PollEvent(&sdl_event))
        {
            switch (sdl_event.type)
            {
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                    check_input(sdl_event);
                    break;
                case SDL_QUIT:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
        }
    }

    SDL_Quit();

    return EXIT_SUCCESS;
}
