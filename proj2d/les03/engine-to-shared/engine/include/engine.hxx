#pragma once
#include <string_view>
#include <unordered_map>

#ifdef WIN32
#ifdef engine_shared03_EXPORTS
#define ENGN_DECLSPEC __declspec(dllexport)
#else
#define ENGN_DECLSPEC __declspec(dllimport)
#endif // engine_shared03_EXPORTS
#else
#define ENGN_DECLSPEC
#endif // WIN32

namespace engn
{
enum class event_action
{
    AHEAD,
    BACK,
    LEFT,
    RIGHT,
    TURN_LEFT,
    TURN_RIGHT,
    TURN_UP,
    TURN_DOWN,
    MOUSE_LEFT_BTN,
    MOUSE_LEFT_DOUBLE_BTN,
    MOUSE_RIGHT_BTN,
    MOUSE_MIDDLE_BTN,
    MOUSE_X1_BTN,
    MOUSE_X2_BTN,
    TOUCH_FINGERDOWN,
    TOUCH_FINGERUP,
    TOUCH_FINGERMOTION,
    QUIT
};

enum class event_type
{
    PRESSED,
    RELEASED,
    MOUSEMOTION,
    MOUSEBUTTONDOWN,
    MOUSEBUTTONUP,
    TOUCH_FINGERDOWN,
    TOUCH_FINGERUP,
    TOUCH_FINGERMOTION,
    QUIT
};

typedef std::pair<engn::event_type, engn::event_action> event_pair;
typedef std::pair<event_type, std::string_view>         game_pair;

class ENGN_DECLSPEC engine
{
public:
    virtual ~engine();
    virtual void initialize()             = 0;
    virtual bool read_input(game_pair& e) = 0;
    virtual void uinitialize()            = 0;
};

ENGN_DECLSPEC engine* create_engine();
ENGN_DECLSPEC void    destroy_engine(engine*);
} // namespace engn
