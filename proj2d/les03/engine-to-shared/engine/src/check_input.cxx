#include <SDL.h>
#include <SDL_keycode.h>
#include <SDL_mouse.h>
#include <algorithm>
#include <array>
#include <engine.hxx>
#include <map>

namespace engn
{
// keys for event searching should be only UPPERCASE, construct: "eventName" +
// + '_' + "eventType" (ex. AHEAD_PRESSED). For QUIT only eventType.
static std::map<event_pair, std::string_view> actions{
    // keyboard key pressed and action AHEAD
    { { event_type::PRESSED, event_action::AHEAD }, "Ahead_pressed" },
    // keyboard key released and action AHEAD
    { { event_type::RELEASED, event_action::AHEAD }, "Ahead_released" },
    // keyboard key pressed and action BACK
    { { event_type::PRESSED, event_action::BACK }, "Back_pressed" },
    // keyboard key released and action BACK
    { { event_type::RELEASED, event_action::BACK }, "Back_released" },
    // keyboard key pressed and action LEFT
    { { event_type::PRESSED, event_action::LEFT }, "Left_pressed" },
    // keyboard key released and action LEFT
    { { event_type::RELEASED, event_action::LEFT }, "Left_released" },
    // keyboard key pressed and action RIGHT
    { { event_type::PRESSED, event_action::RIGHT }, "Right_pressed" },
    // keyboard key released and action RIGHT
    { { event_type::RELEASED, event_action::RIGHT }, "Right_released" },
    // mouse motion events (may be used as one event in future)
    { { event_type::MOUSEMOTION, event_action::TURN_UP }, "Turn_up" },
    { { event_type::MOUSEMOTION, event_action::TURN_DOWN }, "Turn_down" },
    { { event_type::MOUSEMOTION, event_action::TURN_LEFT }, "Turn_left" },
    { { event_type::MOUSEMOTION, event_action::TURN_RIGHT }, "Turn_right" },
    // mouse buttons events
    { { event_type::MOUSEBUTTONDOWN, event_action::MOUSE_X1_BTN },
      "Mouse x1 button pressed" },
    { { event_type::MOUSEBUTTONUP, event_action::MOUSE_X1_BTN },
      "Mouse x1 button released" },
    { { event_type::MOUSEBUTTONDOWN, event_action::MOUSE_X2_BTN },
      "Mouse x2 button pressed" },
    { { event_type::MOUSEBUTTONUP, event_action::MOUSE_X2_BTN },
      "Mouse x2 button released" },
    { { event_type::MOUSEBUTTONDOWN, event_action::MOUSE_LEFT_BTN },
      "Mouse left button pressed" },
    { { event_type::MOUSEBUTTONUP, event_action::MOUSE_LEFT_BTN },
      "Mouse left button released" },
    { { event_type::MOUSEBUTTONDOWN, event_action::MOUSE_RIGHT_BTN },
      "Mouse right button pressed" },
    { { event_type::MOUSEBUTTONUP, event_action::MOUSE_RIGHT_BTN },
      "Mouse right button released" },
    { { event_type::MOUSEBUTTONDOWN, event_action::MOUSE_MIDDLE_BTN },
      "Mouse middle button pressed" },
    { { event_type::MOUSEBUTTONUP, event_action::MOUSE_MIDDLE_BTN },
      "Mouse middle button released" },
    { { event_type::MOUSEBUTTONDOWN, event_action::MOUSE_LEFT_DOUBLE_BTN },
      "Mouse left button double pressed" },
    { { event_type::MOUSEBUTTONUP, event_action::MOUSE_LEFT_DOUBLE_BTN },
      "Mouse left button double released" },
    // Touch events
    { { event_type::TOUCH_FINGERUP, event_action::TOUCH_FINGERUP },
      "Finger up" },
    { { event_type::TOUCH_FINGERDOWN, event_action::TOUCH_FINGERDOWN },
      "Finger down" },
    { { event_type::TOUCH_FINGERMOTION, event_action::TOUCH_FINGERMOTION },
      "Finger motion" },
    // QUIT event triggered
    { { event_type::QUIT, event_action::QUIT }, "Quit" }
};

// struct for keys handling
#pragma pack(push, 4)
struct bind
{
    bind(SDL_Keycode key, event_action action)
        : m_key(key)
        , m_action(action)
    {
    }
    SDL_Keycode  m_key;
    event_action m_action;
};
#pragma pack(pop)

static std::array<bind, 8> handled_keys{ { { SDLK_w, event_action::AHEAD },
                                           { SDLK_UP, event_action::AHEAD },
                                           { SDLK_s, event_action::BACK },
                                           { SDLK_DOWN, event_action::BACK },
                                           { SDLK_a, event_action::LEFT },
                                           { SDLK_LEFT, event_action::LEFT },
                                           { SDLK_d, event_action::RIGHT },
                                           { SDLK_RIGHT,
                                             event_action::RIGHT } } };

class check_input
{
public:
    check_input() {}
    /**
     * @brief operator () - check mouse keyboard event.
     * @param e_type - type of sdl event
     * @param event_key - key that pressed/released
     * @param e - pair of sdl event and reflection
     * @return true if action finded, else false
     */
    bool operator()(const event_type&        e_type,
                    const SDL_KeyboardEvent& event_key, game_pair& e)
    {
        auto prefix = std::find_if(
            std::begin(handled_keys), std::end(handled_keys),
            [&](const bind& b) { return b.m_key == event_key.keysym.sym; });

        if (prefix == end(handled_keys))
        {
            return false;
        }

        e = game_pair(e_type, actions.at(event_pair(e_type, prefix->m_action)));

        return true;
    }
    /**
     * @brief operator () - check mouse motion event.
     * @param e_type - type of sdl event
     * @param motion - sdl mouse motion event (used relative)
     * @param e - pair of sdl event and reflection
     * @return true if action finded, else false
     */
    bool operator()(const event_type&           e_type,
                    const SDL_MouseMotionEvent& motion, game_pair& e)
    {
        event_action action;
        if (motion.xrel != 0)
        {
            if (motion.xrel < 0)
            {
                action = event_action::TURN_LEFT;
            }
            else if (motion.xrel > 0)
            {
                action = event_action::TURN_RIGHT;
            }
        }
        else if (motion.yrel != 0)
        {
            if (motion.yrel < 0)
            {
                action = event_action::TURN_UP;
            }
            else if (motion.yrel > 0)
            {
                action = event_action::TURN_DOWN;
            }
        }
        else
        {
            return false;
        }

        e = game_pair(e_type, actions.at(event_pair(e_type, action)));
        return true;
    }
    /**
     * @brief operator () - check mouse button events.
     * @param e_type - type of sdl event
     * @param button_event - sdl mouse button event struct
     * @param e - pair of sdl event and reflection
     * @return true if action finded, else false
     */
    bool operator()(const event_type&           e_type,
                    const SDL_MouseButtonEvent& button_event, game_pair& e)
    {
        event_action action;
        switch (button_event.button)
        {
            case SDL_BUTTON_LEFT:

                action = (button_event.clicks == 1)
                             ? event_action::MOUSE_LEFT_BTN
                             : event_action::MOUSE_LEFT_DOUBLE_BTN;
                break;
            case SDL_BUTTON_MIDDLE:
                action = event_action::MOUSE_MIDDLE_BTN;
                break;
            case SDL_BUTTON_RIGHT:
                action = event_action::MOUSE_RIGHT_BTN;
                break;
            case SDL_BUTTON_X1:
                action = event_action::MOUSE_X1_BTN;
                break;
            case SDL_BUTTON_X2:
                action = event_action::MOUSE_X2_BTN;
                break;
            default:
                return false;
        }
        e = game_pair(e_type, actions.at(event_pair(e_type, action)));
        return true;
    }
    bool operator()(const event_type&           e_type,
                    const SDL_TouchFingerEvent& touch_event, game_pair& e)
    {
        event_action action;
        switch (touch_event.type)
        {
            case SDL_FINGERUP:
                action = event_action::TOUCH_FINGERUP;
                break;
            case SDL_FINGERDOWN:
                action = event_action::TOUCH_FINGERDOWN;
                break;
            case SDL_FINGERMOTION:
                action = event_action::TOUCH_FINGERMOTION;
                break;
            default:
                return false;
        }
        e = game_pair(e_type, actions.at(event_pair(e_type, action)));
        return true;
    }
};
} // namespace engn
