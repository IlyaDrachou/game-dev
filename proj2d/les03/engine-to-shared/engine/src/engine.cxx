#include "engine.hxx"
#include "check_input.cxx"
#include <SDL.h>
#include <SDL_keycode.h>
#include <SDL_mouse.h>
#include <SDL_version.h>
#include <array>
#include <iostream>
#include <stdexcept>
#include <string>
#include <string_view>

namespace engn
{
class engine_impl final : public engine
{
    engine_impl() {}

public:
    friend engine* create_engine(); // For anti-pattern singlton
    void           initialize() override final
    {
        SDL_version linked{};
        SDL_GetVersion(&linked);

        if (SDL_COMPILEDVERSION !=
            SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
        {
            throw std::runtime_error(
                "Versions (compiled and linked) should be the same!!!");
        }

        auto init_result = SDL_Init(SDL_INIT_EVERYTHING);
        if (init_result != 0)
        {
            std::string what{ "Can't init SDL: " };
            throw std::runtime_error(what + SDL_GetError());
        }

        window = SDL_CreateWindow("First SDL window",      // window title
                                  SDL_WINDOWPOS_UNDEFINED, // initial x position
                                  SDL_WINDOWPOS_UNDEFINED, // initial y position
                                  640,                     // width, in pixels
                                  480,                     // height, in pixels
                                  SDL_WINDOW_OPENGL        // flags - see below
        );

        if (window == nullptr)
        {
            std::string what{ "Can't create window: " };
            throw std::runtime_error(what + SDL_GetError());
        }

        // For relative mouse motion
        // SDL_SetRelativeMouseMode(SDL_TRUE);
    }
    bool read_input(game_pair& e) override final
    {
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event))
        {
            check_input check;
            switch (sdl_event.type)
            {
                    //                case SDL_CONTROLLERAXISMOTION:
                    //                    std::cout << "sdl_event.caxis.value"
                    //                    << std::endl; return true;
                case SDL_QUIT:
                    e = game_pair(event_type::QUIT,
                                  actions.at(event_pair(event_type::QUIT,
                                                        event_action::QUIT)));
                    return true;
                case SDL_KEYDOWN:
                    return check(event_type::PRESSED, sdl_event.key, e);
                case SDL_KEYUP:
                    return check(event_type::RELEASED, sdl_event.key, e);
                case SDL_MOUSEBUTTONDOWN:
                    return check(event_type::MOUSEBUTTONDOWN, sdl_event.button,
                                 e);
                case SDL_MOUSEBUTTONUP:
                    return check(event_type::MOUSEBUTTONUP, sdl_event.button,
                                 e);
                case SDL_MOUSEMOTION:
                    return check(event_type::MOUSEMOTION, sdl_event.motion, e);
                case SDL_FINGERUP:
                    return check(event_type::TOUCH_FINGERUP, sdl_event.tfinger,
                                 e);
                case SDL_FINGERDOWN:
                    return check(event_type::TOUCH_FINGERDOWN,
                                 sdl_event.tfinger, e);
                case SDL_FINGERMOTION:
                    return check(event_type::TOUCH_FINGERMOTION,
                                 sdl_event.tfinger, e);
            }
        }
        return false;
    }
    void uinitialize() override final { SDL_Quit(); }

private:
    SDL_Window* window{ nullptr };
};
engine::~engine() {}

static engine* e = nullptr;
/**
 * @brief create_engine
 * @return pointer on engine
 */
engine* create_engine()
{
    if (e != nullptr)
    {
        throw std::runtime_error("Engine already exists!!!");
    }
    e = new engine_impl;
    return e;
}

void destroy_engine(engine* e)
{
    if (e == nullptr)
    {
        throw std::runtime_error("Engine already destroyed!!!");
    }
    delete e;
}

} // namespace engn
