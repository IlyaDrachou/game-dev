#pragma once
#include "triangle_indexed_renderer.hxx"

namespace triangle_space
{
struct vertex
{
    /// x
    double f0{ 0 };
    /// y
    double f1{ 0 };
    /// red
    double f2{ 0 };
    /// green
    double f3{ 0 };
    /// blue
    double f4{ 0 };
    /// u
    double f5{ 0 };
    /// v
    double f6{ 0 };
    /// ?
    double f7{ 0 };
};

double interpolate(const double f0, const double f1, const double t);

vertex interpolate(const vertex& v0, const vertex& v1, const double t);

struct uniforms
{
    double f0{ 0 };
    double f1{ 0 };
    double f2{ 0 };
    double f3{ 0 };
    double f4{ 0 };
    double f5{ 0 };
    double f6{ 0 };
    double f7{ 0 };
};

struct gfx_program
{
    virtual ~gfx_program()                                        = default;
    virtual void                set_uniforms(const uniforms&)     = 0;
    virtual vertex              vertex_shader(const vertex& v_in) = 0;
    virtual canvas_space::color pixel_shader(const vertex& v_in)  = 0;
};

class triangle_interpolated_renderer : public triangle_indexed_renderer
{
public:
    triangle_interpolated_renderer(canvas_space::canvas& canvas, size_t width,
                                   size_t height);
    void set_gfx_program(gfx_program& program);
    void draw_triangles(std::vector<vertex>&  vertexes,
                        std::vector<uint8_t>& indexes);

protected:
    std::vector<vertex> rasterize_triangle(const vertex& v0, const vertex& v1,
                                           const vertex& v2);
    std::vector<vertex> raster_horizontal_triangle(const vertex& single,
                                                   const vertex& left,
                                                   const vertex& right);
    void                raster_one_horizontal_line(const vertex&        left_vertex,
                                                   const vertex&        right_vertex,
                                                   std::vector<vertex>& out);

private:
    gfx_program* m_program = nullptr;
};
} // namespace triangle_space
