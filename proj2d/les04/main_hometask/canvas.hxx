#pragma once
#include <array>
#include <cstdint>
#include <fstream>
#include <string>
#include <vector>

namespace canvas_space
{
constexpr size_t width{ 640 };
constexpr size_t height{ 480 };
constexpr size_t buffer_size{ width * height };

#pragma pack(push, 1)
struct color
{
    uint8_t red{};
    uint8_t green{};
    uint8_t blue{};
    bool    operator==(const color& another) const;
};
#pragma pack(pop)

class canvas : public std::array<color, buffer_size>
{
public:
    void save_image(const std::string& filename);
    void load_image(const std::string& filename);
};

struct position
{
    double   length();
    position operator-(const position& another);
    int32_t  x{};
    int32_t  y{};
};

using pixels_position = std::vector<position>;

struct irender
{
    virtual void            clear(color)                             = 0;
    virtual void            set_pixel(position, color)               = 0;
    virtual pixels_position pixels_pos(position start, position end) = 0;

    virtual ~irender() = default;
};
} // namespace canvas_space
