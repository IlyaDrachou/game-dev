#include "line_renderer.hxx"
#include <algorithm>

namespace line_space
{
line_renderer::line_renderer(canvas_space::canvas& _canvas, size_t _width,
                             size_t _height)
    : canvas(_canvas)
    , width(_width)
    , height(_height)
{
}

void line_renderer::clear(canvas_space::color color)
{
    std::fill(std::begin(canvas), std::end(canvas), color);
}

void line_renderer::set_pixel(canvas_space::position position,
                              canvas_space::color    color)
{
    const size_t pixel_position{ static_cast<size_t>(position.y) * width +
                                 static_cast<size_t>(position.x) };
    canvas.at(pixel_position) = color;
}

canvas_space::pixels_position line_renderer::pixels_pos(
    canvas_space::position start, canvas_space::position end)
{
    canvas_space::pixels_position positions;
    auto                          x0 = start.x;
    auto                          y0 = start.y;
    auto                          x1 = end.x;
    auto                          y1 = end.y;
    int                           deltax{ std::abs(x1 - x0) };
    int                           deltay{ std::abs(y1 - y0) };
    // Bresenham's algoritm

    auto bresenham_low = [&](int x0, int y0, int x1, int y1) {
        int error{};
        int deltaerr = deltay;
        int y        = y0;
        int diry     = y1 - y0;

        if (diry > 0)
        {
            diry = 1;
        }
        else if (diry < 0)
        {
            diry = -1;
        }
        for (int x = x0; x <= x1; x++)
        {
            positions.push_back(canvas_space::position{ x, y });
            error += deltaerr;
            if (2 * error >= deltax)
            {
                y += diry;
                error -= deltax;
            }
        }
    };

    auto bresenham_high = [&](int x0, int y0, int x1, int y1) {
        int error{};
        int deltaerr = deltay;
        int x        = x0;
        int dirx     = x1 - x0;

        if (dirx > 0)
        {
            dirx = 1;
        }
        else if (dirx < 0)
        {
            dirx = -1;
        }
        for (int y = y0; y <= y1; y++)
        {
            positions.push_back(canvas_space::position{ x, y });
            error += deltaerr;
            if (2 * error >= deltax)
            {
                x += dirx;
                error -= deltax;
            }
        }
    };

    if (deltay < deltax)
    {
        if (x0 > x1)
        {
            bresenham_low(x1, y1, x0, y0);
        }
        else
        {
            bresenham_low(x0, y0, x1, y1);
        }
    }
    else
    {
        if (y0 > y1)
        {
            bresenham_high(x1, y1, x0, y0);
        }
        else
        {
            bresenham_high(x0, y0, x1, y1);
        }
    }

    return positions;
}

void line_renderer::draw_line(canvas_space::position start,
                              canvas_space::position end,
                              canvas_space::color    color)
{
    canvas_space::pixels_position l = pixels_pos(start, end);
    std::for_each(std::begin(l), std::end(l),
                  [&](auto& pos) { set_pixel(pos, color); });
}

} // namespace line_space
