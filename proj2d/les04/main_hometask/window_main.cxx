#include <SDL.h>
#include <SDL_mouse.h>
#include <iostream>

#include "triangle_interpolated_renderer.hxx"

int main(int, char**)
{
    using namespace std;

    SDL_version linked{};
    SDL_GetVersion(&linked);

    cout << (SDL_COMPILEDVERSION ==
             SDL_VERSIONNUM(linked.major, linked.minor, linked.patch));

    auto init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        cout << "Can't init SDL: " << SDL_GetError();
        return EXIT_FAILURE;
    }

    auto window =
        SDL_CreateWindow("Soft render",           // window title
                         SDL_WINDOWPOS_UNDEFINED, // initial x position
                         SDL_WINDOWPOS_UNDEFINED, // initial y position
                         canvas_space::width,     // width, in pixels
                         canvas_space::height,    // height, in pixels
                         SDL_WINDOW_OPENGL        // flags - see below
        );
    if (window == nullptr)
    {
        cout << "Can't create window: " << SDL_GetError();
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    const canvas_space::color black = { 0, 0, 0 };

    canvas_space::canvas image;

    triangle_space::triangle_interpolated_renderer interpolated_renderer(
        image, canvas_space::width, canvas_space::height);

    struct program : triangle_space::gfx_program
    {
        double mouse_x{};
        double mouse_y{};
        double radius{};

        void set_uniforms(const triangle_space::uniforms& a_uniforms) override
        {
            mouse_x = a_uniforms.f0;
            mouse_y = a_uniforms.f1;
            radius  = a_uniforms.f2;
        }
        triangle_space::vertex vertex_shader(
            const triangle_space::vertex& v_in) override
        {
            triangle_space::vertex out = v_in;

            double x = out.f0;
            double y = out.f1;

            out.f0 = x;
            out.f1 = y;

            return out;
        }
        canvas_space::color pixel_shader(
            const triangle_space::vertex& v_in) override
        {
            canvas_space::color out;
            out.red   = static_cast<uint8_t>(v_in.f2 * 255);
            out.green = static_cast<uint8_t>(v_in.f3 * 255);
            out.blue  = static_cast<uint8_t>(v_in.f4 * 255);

            double x  = v_in.f0;
            double y  = v_in.f1;
            double dx = mouse_x - x;
            double dy = mouse_y - y;
            if (dx * dx + dy * dy < radius * radius)
            {
                // make pixel gray if mouse cursor around current pixel with
                // radius
                // gray scale with formula: 0.21 R + 0.72 G + 0.07 B.
                auto gray = static_cast<uint8_t>(
                    0.21 * out.red + 0.72 * out.green + 0.07 * out.blue);
                out.red   = gray;
                out.green = gray;
                out.blue  = gray;
            }
            return out;
        }
    } program01;

    interpolated_renderer.clear(black);
    interpolated_renderer.set_gfx_program(program01);

    std::vector<triangle_space::vertex> triangle_v{
        { 0, 0, 1, 1, 0, 0, 0, 0 },
        { 0, canvas_space::height - 1, 0, 1, 0, 0, canvas_space::height - 1,
          0 },
        { canvas_space::width - 1, canvas_space::height - 1, 1, 0, 1,
          canvas_space::width - 1, canvas_space::height - 1, 0 },
        { canvas_space::width - 1, 0, 1, 0, 0, canvas_space::width - 1, 0, 0 }
    };
    std::vector<uint8_t> indexes_v{ 0, 1, 3, 3, 1, 2 };

    void*     pixels = image.data();
    const int depth  = sizeof(canvas_space::color) * 8;
    const int pitch  = canvas_space::width * sizeof(canvas_space::color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    interpolated_renderer.set_gfx_program(program01);

    double mouse_x{};
    double mouse_y{};
    double radius{ 20.0 }; // 20 pixels radius

    bool continue_loop{ true };
    while (continue_loop)
    {
        SDL_Event sdl_event;
        while (SDL_PollEvent(&sdl_event))
        {
            switch (sdl_event.type)
            {
                case SDL_MOUSEMOTION:
                    mouse_x = sdl_event.motion.x;
                    mouse_y = sdl_event.motion.y;
                    break;
                case SDL_QUIT:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
        }
        interpolated_renderer.clear(black);
        program01.set_uniforms(
            triangle_space::uniforms{ mouse_x, mouse_y, radius });

        interpolated_renderer.draw_triangles(triangle_v, indexes_v);

        SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
            pixels, canvas_space::width, canvas_space::height, depth, pitch,
            rmask, gmask, bmask, amask);
        if (bitmapSurface == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_Texture* bitmapTex =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_FreeSurface(bitmapSurface);

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapTex);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
