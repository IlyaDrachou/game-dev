#include "triangle_interpolated_renderer.hxx"
#include <algorithm>
#include <assert.h>
#include <cmath>

namespace triangle_space
{

double interpolate(const double f0, const double f1, const double t)
{
    assert(t >= 0);
    assert(t <= 1);
    return (1 - t) * f0 + t * f1;
}

vertex interpolate(const vertex& v0, const vertex& v1, const double t)
{
    return { interpolate(v0.f0, v1.f0, t), interpolate(v0.f1, v1.f1, t),
             interpolate(v0.f2, v1.f2, t), interpolate(v0.f3, v1.f3, t),
             interpolate(v0.f4, v1.f4, t), interpolate(v0.f5, v1.f5, t),
             interpolate(v0.f6, v1.f6, t), interpolate(v0.f7, v1.f7, t) };
}

triangle_interpolated_renderer::triangle_interpolated_renderer(
    canvas_space::canvas& canvas, size_t width, size_t height)
    : triangle_indexed_renderer(canvas, width, height)
{
}

void triangle_interpolated_renderer::set_gfx_program(gfx_program& program)
{
    m_program = &program;
}

void triangle_interpolated_renderer::draw_triangles(
    std::vector<vertex>& vertexes, std::vector<uint8_t>& indexes)
{
    for (size_t i{ 0 }; i < indexes.size(); i += 3)
    {
        auto index0{ indexes.at(i) };
        auto index1{ indexes.at(i + 1) };
        auto index2{ indexes.at(i + 2) };

        auto v0 = vertexes.at(index0);
        auto v1 = vertexes.at(index1);
        auto v2 = vertexes.at(index2);

        const vertex v0_ = m_program->vertex_shader(v0);
        const vertex v1_ = m_program->vertex_shader(v1);
        const vertex v2_ = m_program->vertex_shader(v2);

        const std::vector<vertex> triangle{ rasterize_triangle(v0_, v1_, v2_) };
        for (auto pixel : triangle)
        {
            const auto                   color = m_program->pixel_shader(pixel);
            const canvas_space::position pos{
                static_cast<int32_t>(std::round(pixel.f0)),
                static_cast<int32_t>(std::round(pixel.f1))
            };
            set_pixel(pos, color);
        }
    }
}

std::vector<vertex> triangle_interpolated_renderer::rasterize_triangle(
    const vertex& v0, const vertex& v1, const vertex& v2)
{
    std::vector<vertex> pixels;

    std::vector<vertex> vertexes = { v0, v1, v2 };
    std::sort(std::begin(vertexes), std::end(vertexes),
              [&](const vertex& left, const vertex& right) {
                  return left.f1 < right.f1;
              });

    auto bottom = vertexes.at(0);
    auto middle = vertexes.at(1);
    auto top    = vertexes.at(2);

    // y = kx + b, where k = dy/dx
    auto dx{ top.f0 - bottom.f0 };
    auto dy{ top.f1 - bottom.f1 };
    if (dx == 0.0)
    {
        auto t{ std::abs((top.f1 - middle.f1) / dy) };
        auto middle2{ interpolate(top, bottom, t) };
        auto top_triangle = raster_horizontal_triangle(top, middle, middle2);
        pixels.insert(std::end(pixels), std::begin(top_triangle),
                      std::end(top_triangle));
        auto bottom_tringle =
            raster_horizontal_triangle(bottom, middle, middle2);
        pixels.insert(std::end(pixels), std::begin(bottom_tringle),
                      std::end(bottom_tringle));

        return pixels;
    }
    if (dy == 0.0)
    {
        auto tringle = raster_horizontal_triangle(top, middle, bottom);
        pixels.insert(std::end(pixels), std::begin(tringle), std::end(tringle));
        return pixels;
    }
    auto k = dy / dx;                          // line slope
    auto b = (((-top.f0) * dy) / dx) + top.f1; // y - offset
    auto x = (middle.f1 - b) / k; // x coordinate opposite to middle vertex
    auto top_to_middle_length =
        std::sqrt(std::pow(top.f0 - x, 2) + std::pow(top.f1 - middle.f1, 2));
    auto top_to_bottom_lendth = std::sqrt(std::pow(dx, 2) + std::pow(dy, 2));
    auto t{ top_to_middle_length / top_to_bottom_lendth };

    auto middle2{ interpolate(top, bottom,
                              t) }; // get vertex in given coordinates
    auto top_triangle = raster_horizontal_triangle(top, middle, middle2);
    pixels.insert(std::end(pixels), std::begin(top_triangle),
                  std::end(top_triangle));
    auto bottom_tringle = raster_horizontal_triangle(bottom, middle, middle2);
    pixels.insert(std::end(pixels), std::begin(bottom_tringle),
                  std::end(bottom_tringle));

    return pixels;
}

std::vector<vertex> triangle_interpolated_renderer::raster_horizontal_triangle(
    const vertex& single, const vertex& left, const vertex& right)
{
    std::vector<vertex> pixels;

    auto horizontal_lines_count{ static_cast<size_t>(
        std::round(std::abs(single.f1 - left.f1))) };

    if (horizontal_lines_count > 0)
    {
        for (size_t i{ 0 }; i < horizontal_lines_count + 1; i++)
        {
            double t{ static_cast<double>(i) / horizontal_lines_count };
            auto   left_vertex{ interpolate(single, left, t) };
            auto   right_vertex{ interpolate(single, right, t) };
            raster_one_horizontal_line(left_vertex, right_vertex, pixels);
        }
    }
    else
    {
        raster_one_horizontal_line(left, right, pixels);
    }
    return pixels;
}

void triangle_interpolated_renderer::raster_one_horizontal_line(
    const vertex& left_vertex, const vertex& right_vertex,
    std::vector<vertex>& out)
{
    size_t pixels_count{ static_cast<size_t>(
        std::round(std::abs(left_vertex.f0 - right_vertex.f0))) };
    if (pixels_count > 0)
    {
        for (size_t i{ 0 }; i <= pixels_count + 1; i++)
        {
            double t{ static_cast<double>(i) / (pixels_count + 1) };
            auto   pixel{ interpolate(left_vertex, right_vertex, t) };
            out.push_back(pixel);
        }
    }
    else
    {
        out.push_back(left_vertex);
    }
}

} // namespace triangle_space
