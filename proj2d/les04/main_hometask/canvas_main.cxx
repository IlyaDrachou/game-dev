#include "canvas.hxx"

int main(int, char**)
{
    canvas_space::color orange{ 255, 130, 0 };
    canvas_space::color black{ 0, 0, 0 };

    canvas_space::canvas canvas;
    std::fill(std::begin(canvas), std::end(canvas), orange);
    canvas.save_image("canvas_main_save.ppm");
    std::fill(std::begin(canvas), std::end(canvas), black);
    canvas.save_image("canvas_main_load.ppm");
    canvas.load_image("canvas_main_save.ppm");
    canvas.save_image("canvas_main_load_save.ppm");
    return 0;
}
