#include "canvas.hxx"
#include "line_renderer.hxx"

int main(int, char**)
{
    canvas_space::color black{ 0, 0, 0 };

    canvas_space::canvas      canvas;
    line_space::line_renderer line(canvas, canvas_space::width,
                                   canvas_space::height);
    line.clear(black);
    for (int i = 0; i < 100; i++)
    {
        auto point1 = canvas_space::position{ rand() % 640, rand() % 480 };
        auto point2 = canvas_space::position{ rand() % 640, rand() % 480 };
        auto color  = canvas_space::color{ static_cast<uint8_t>(rand() % 256),
                                          static_cast<uint8_t>(rand() % 256),
                                          static_cast<uint8_t>(rand() % 256) };
        line.draw_line(point1, point2, color);
    }

    canvas.save_image("line_main.ppm");
    return 0;
}
