#include "triangle_interpolated_renderer.hxx"
#include <cmath>

int main(int, char**)
{
    using namespace canvas_space;
    using namespace triangle_space;
    const color black = { 0, 0, 0 };

    canvas image;

    triangle_interpolated_renderer interpolated_render(image, width, height);

    struct program : gfx_program
    {
        void   set_uniforms(const uniforms&) override {}
        vertex vertex_shader(const vertex& v_in) override
        {
            vertex out = v_in;

            // rotate
            double alpha = 3.14159 / 6; // 30 degree
            double x     = out.f0;
            double y     = out.f1;
            out.f0       = x * std::cos(alpha) - y * std::sin(alpha);
            out.f1       = x * std::sin(alpha) + y * std::cos(alpha);

            // scale into 3 times
            out.f0 *= 0.3;
            out.f1 *= 0.3;

            // move
            out.f0 += (width / 2);
            out.f1 += (height / 2);

            return out;
        }
        color pixel_shader(const vertex& v_in) override
        {
            color out;
            out.red   = static_cast<uint8_t>(v_in.f2 * 255);
            out.green = static_cast<uint8_t>(v_in.f3 * 255);
            out.blue  = static_cast<uint8_t>(v_in.f4 * 255);
            return out;
        }
    } program01;

    interpolated_render.clear(black);
    interpolated_render.set_gfx_program(program01);

    std::vector<vertex> triangle_v{
        { canvas_space::width - 1, 0, 1, 0, 0, canvas_space::width - 1, 0, 0 },
        { 0, canvas_space::height - 1, 0, 1, 0, 0, canvas_space::height - 1,
          0 },
        { canvas_space::width - 1, canvas_space::height - 1, 0, 0, 1,
          canvas_space::width - 1, canvas_space::height - 1, 0 }
    };
    std::vector<uint8_t> indexes_v{ 0, 1, 2 };

    interpolated_render.draw_triangles(triangle_v, indexes_v);

    image.save_image("triangle_interpolated.ppm");

    // texture example
    //    struct program_tex : gfx_program
    //    {
    //        std::array<color, buffer_size> texture;

    //        void   set_uniforms(const uniforms&) override {}
    //        vertex vertex_shader(const vertex& v_in) override
    //        {
    //            vertex out = v_in;
    //            return out;
    //        }
    //        color pixel_shader(const vertex& v_in) override
    //        {
    //            color out;

    //            out.red   = static_cast<uint8_t>(v_in.f2 * 255);
    //            out.green = static_cast<uint8_t>(v_in.f3 * 255);
    //            out.blue  = static_cast<uint8_t>(v_in.f4 * 255);

    //            color from_texture = sample2d(v_in.f5, v_in.f6);
    //            out.red += from_texture.red;
    //            out.green = from_texture.green;
    //            out.blue += from_texture.blue;
    //            return out;
    //        }

    //        void set_texture(const std::array<color, buffer_size>& tex)
    //        {
    //            texture = tex;
    //        }

    //        color sample2d(double u_, double v_)
    //        {
    //            uint32_t u = static_cast<uint32_t>(std::round(u_));
    //            uint32_t v = static_cast<uint32_t>(std::round(v_));

    //            color c = texture.at(v * width + u);
    //            return c;
    //        }
    //    } program02;

    //    program02.set_texture(image);

    //    interpolated_render.set_gfx_program(program02);

    //    interpolated_render.clear(black);

    //    interpolated_render.draw_triangles(triangle_v, indexes_v);

    //    image.save_image("06_textured_triangle.ppm");

    return 0;
}
