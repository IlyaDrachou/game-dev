#include "canvas.hxx"
#include <cmath>

namespace canvas_space
{

bool color::operator==(const color& another) const
{
    return (red == another.red && green == another.green &&
            blue == another.blue);
}

void canvas::save_image(const std::string& filename)
{
    std::ofstream out_file;
    out_file.exceptions(std::ios_base::failbit);
    out_file.open(filename, std::ios_base::binary);
    out_file << "P6\n" << width << ' ' << height << ' ' << 255 << '\n';
    std::streamsize streamsize{ static_cast<std::streamsize>(sizeof(color) *
                                                             size()) };
    out_file.write(reinterpret_cast<const char*>(this), streamsize);
}

void canvas::load_image(const std::string& filename)
{
    std::ifstream in_file;
    in_file.exceptions(std::ios_base::failbit);
    in_file.open(filename, std::ios_base::binary);
    std::string header;
    size_t      image_width  = 0;
    size_t      image_height = 0;
    std::string color_format;
    in_file >> header >> image_width >> image_height >> color_format >> std::ws;
    if (size() != image_height * image_width)
    {
        throw std::runtime_error("image size not match");
    }
    std::streamsize streamsize{ static_cast<std::streamsize>(sizeof(color) *
                                                             size()) };
    in_file.read(reinterpret_cast<char*>(this), streamsize);
}

constexpr size_t color_size = sizeof(color);

static_assert(3 == color_size, "24 bit per pixel(r,g,b)");

double position::length()
{
    return std::sqrt(x * x + y * y);
}

position position::operator-(const position& another)
{
    return { x - another.x, y - another.y };
}

} // namespace canvas_space
