#include "triangle_indexed_renderer.hxx"

namespace triangle_space
{

triangle_indexed_renderer::triangle_indexed_renderer(
    canvas_space::canvas& canvas, size_t width, size_t height)
    : triangle_renderer(canvas, width, height)
{
}

void triangle_indexed_renderer::draw_triangles(
    canvas_space::pixels_position& vertexes, std::vector<uint8_t>& indexes,
    canvas_space::color color)
{
    using namespace canvas_space;
    pixels_position edge_pixels;

    for (size_t i{ 0 }; i < indexes.size(); i += 3)
    {
        auto index0{ indexes.at(i) };
        auto index1{ indexes.at(i + 1) };
        auto index2{ indexes.at(i + 2) };

        position v0 = vertexes.at(index0);
        position v1 = vertexes.at(index1);
        position v2 = vertexes.at(index2);
        auto     triangle_edge_pixels{ pixels_pos(v0, v1, v2) };
        edge_pixels.insert(std::end(edge_pixels),
                           std::begin(triangle_edge_pixels),
                           std::end(triangle_edge_pixels));
    }
    for (auto pos : edge_pixels)
    {
        set_pixel(pos, color);
    }
}

} // namespace triangle_space
