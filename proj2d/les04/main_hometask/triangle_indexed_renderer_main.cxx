#include "triangle_indexed_renderer.hxx"

int main(int, char**)
{
    canvas_space::color orange{ 255, 130, 0 };
    canvas_space::color black{ 0, 0, 0 };

    canvas_space::canvas                      canvas;
    triangle_space::triangle_indexed_renderer triangle(
        canvas, canvas_space::width, canvas_space::height);
    triangle.clear(black);
    //    canvas_space::pixels_position single_triangle_positions{
    //        { 0, 0 },
    //        { 0, canvas_space::height - 1 },
    //        { canvas_space::width - 1, 0 }
    //    };
    //    triangle.draw_triangle(single_triangle_positions, 3, orange);

    canvas_space::pixels_position triangles_vertixes;
    std::vector<uint8_t>          indexes;

    size_t points_x_count{ 10 };
    size_t points_y_count{ 10 };
    double step_x{ static_cast<double>((canvas_space::width - 1) /
                                       points_x_count) };
    double step_y{ static_cast<double>((canvas_space::height - 1) /
                                       points_y_count) };
    for (double x{ 0 }; x <= canvas_space::width; x += step_x)
    {
        for (double y{ 0 }; y <= canvas_space::height; y += step_y)
        {
            canvas_space::position v{ static_cast<int>(x),
                                      static_cast<int>(y) };

            triangles_vertixes.push_back(v);
        }
    }
    for (size_t x{ 0 }; x < points_x_count; x++)
    {
        for (size_t y{ 0 }; y < points_y_count; y++)
        {
            uint8_t index0 = static_cast<uint8_t>(y * (points_x_count + 1) + x);
            uint8_t index1 = index0 + 1;
            uint8_t index2 =
                static_cast<uint8_t>(index0 + (points_y_count + 1));
            uint8_t index3 = index2 + 1;

            indexes.push_back(index0);
            indexes.push_back(index1);
            indexes.push_back(index3);

            indexes.push_back(index0);
            indexes.push_back(index2);
            indexes.push_back(index3);
        }
    }

    triangle.draw_triangles(triangles_vertixes, indexes, orange);
    canvas.save_image("triangle_indexed_multiple.ppm");

    return EXIT_SUCCESS;
}
