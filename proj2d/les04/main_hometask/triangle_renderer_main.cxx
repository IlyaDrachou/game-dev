#include "triangle_renderer.hxx"

int main(int, char**)
{
    canvas_space::color orange{ 255, 130, 0 };
    canvas_space::color black{ 0, 0, 0 };

    canvas_space::canvas              canvas;
    triangle_space::triangle_renderer triangle(canvas, canvas_space::width,
                                               canvas_space::height);
    triangle.clear(black);
    //    canvas_space::pixels_position single_triangle_positions{
    //        { 0, 0 },
    //        { 0, canvas_space::height - 1 },
    //        { canvas_space::width - 1, 0 }
    //    };
    //    triangle.draw_triangle(single_triangle_positions, 3, orange);

    canvas_space::pixels_position triangles;

    size_t points_x_count{ 10 };
    size_t points_y_count{ 10 };
    double step_x{ static_cast<double>((canvas_space::width - 1) /
                                       points_x_count) };
    double step_y{ static_cast<double>((canvas_space::height - 1) /
                                       points_y_count) };
    for (double x{ 0 }; x <= canvas_space::width - step_x; x += step_x)
    {
        for (double y{ 0 }; y <= canvas_space::height - step_y; y += step_y)
        {
            canvas_space::position v0{ static_cast<int>(x),
                                       static_cast<int>(y) };
            canvas_space::position v1{ static_cast<int>(x + step_x),
                                       static_cast<int>(y) };
            canvas_space::position v2{ static_cast<int>(x),
                                       static_cast<int>(y + step_y) };
            canvas_space::position v3{ static_cast<int>(x + step_x),
                                       static_cast<int>(y + step_y) };

            triangles.insert(std::end(triangles), { v0, v1, v3 });
            triangles.insert(std::end(triangles), { v0, v2, v3 });
        }
    }

    triangle.draw_triangles(triangles, triangles.size(), orange);
    canvas.save_image("triangle_multiple.ppm");

    return EXIT_SUCCESS;
}
