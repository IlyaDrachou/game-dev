#include "triangle_renderer.hxx"

namespace triangle_space
{

triangle_renderer::triangle_renderer(canvas_space::canvas& canvas, size_t width,
                                     size_t height)
    : line_space::line_renderer(canvas, width, height)
{
}

canvas_space::pixels_position triangle_renderer::pixels_pos(
    canvas_space::position v0, canvas_space::position v1,
    canvas_space::position v2)
{
    auto ab = line_renderer::pixels_pos(v0, v1);
    auto ac = line_renderer::pixels_pos(v0, v2);
    auto bc = line_renderer::pixels_pos(v1, v2);

    canvas_space::pixels_position positions;
    positions.insert(std::end(positions), std::begin(ab), std::end(ab));
    positions.insert(std::end(positions), std::begin(ac), std::end(ac));
    positions.insert(std::end(positions), std::begin(bc), std::end(bc));

    return positions;
}

void triangle_renderer::draw_triangles(canvas_space::pixels_position& vertexes,
                                       size_t              vertexes_count,
                                       canvas_space::color color)
{
    using namespace canvas_space;
    pixels_position edge_pixels;

    for (size_t i{ 0 }; i < vertexes_count; i += 3)
    {
        position v0 = vertexes.at(i);
        position v1 = vertexes.at(i + 1);
        position v2 = vertexes.at(i + 2);
        auto     triangle_edge_pixels{ pixels_pos(v0, v1, v2) };
        edge_pixels.insert(std::end(edge_pixels),
                           std::begin(triangle_edge_pixels),
                           std::end(triangle_edge_pixels));
    }
    for (auto pos : edge_pixels)
    {
        set_pixel(pos, color);
    }
}

} // namespace triangle_space
