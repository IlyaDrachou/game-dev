#include "include/canvas.hxx"
#include "include/triangle_interpolated_renderer.hxx"

#include <SDL.h>

#include <cstdlib>
#include <iostream>

int main(int, char**)
{
    using namespace std;

    if (0 != SDL_Init(SDL_INIT_EVERYTHING))
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Window* window = SDL_CreateWindow(
        "runtime soft render", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        canvas_space::width, canvas_space::height, SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    const canvas_space::color black = { 0, 0, 0 };

    canvas_space::canvas image;

    triangle_space::triangle_interpolated_renderer interpolated_render(
        image, canvas_space::width, canvas_space::height);

    struct program : triangle_space::gfx_program
    {
        double mouse_x{};
        double mouse_y{};
        double radius{};

        const int factor{ 3 };

        void set_uniforms(const triangle_space::uniforms& a_uniforms) override
        {
            mouse_x = a_uniforms.f0;
            mouse_y = a_uniforms.f1;
            radius  = a_uniforms.f2;
        }
        triangle_space::vertex vertex_shader(
            const triangle_space::vertex& v_in) override
        {
            triangle_space::vertex out = v_in;

            double x = out.f0;
            double y = out.f1;

            double dx = x - mouse_x;
            double dy = y - mouse_y;

            if (out.f7 == 0.0)
            {

                double dif_x{};
                double dif_y{};
                for (int i{ 1 }; i <= factor; i++)
                {
                    if (dx * dx + dy * dy < i * i * radius * radius)
                    {
                        if (dx != 0.0)
                        {
                            double angle{ std::atan(dy / dx) };
                            auto   dif{ std::abs(i * radius -
                                               std::sqrt(dx * dx + dy * dy)) };
                            dif_x = dif * std::cos(angle);
                            dif_x = (dx < 0) ? -dif_x : dif_x;
                            dif_y = dif * std::sin(angle);
                            dif_y = (dx < 0) ? -dif_y : dif_y;
                        }
                        else
                        {
                            auto dif{ i * radius -
                                      std::sqrt(dx * dx + dy * dy) };
                            dif_y = (dy < 0) ? -dif : dif;
                        }

                        out.f2 = i / factor;
                        out.f3 = 1 - i / factor;
                    }
                }
                out.f0 += dif_x / factor;
                out.f1 += dif_y / factor;
            }

            return out;
        }
        canvas_space::color pixel_shader(
            const triangle_space::vertex& v_in) override
        {
            triangle_space::vertex out = v_in;

            double x = out.f0;
            double y = out.f1;

            double dx = x - mouse_x;
            double dy = y - mouse_y;

            double distance{};
            for (int i{ 1 }; i <= factor; i++)
            {
                if (dx * dx + dy * dy < factor * i * i * radius * radius)
                {
                    distance =
                        std::sqrt(dx * dx + dy * dy) / (factor * i * radius);
                }
            }
            canvas_space::color out_color;
            if (distance == 0.0)
            {
                out_color.red   = 0;
                out_color.green = static_cast<uint8_t>(255);
            }
            else
            {
                out_color.red   = static_cast<uint8_t>((1 - distance) * 255);
                out_color.green = static_cast<uint8_t>(distance * 255);
            }

            return out_color;
        }
    } program01;

    size_t dot_in_line_count{ 20 };
    auto step_x{ static_cast<double>(canvas_space::width) / dot_in_line_count };
    auto step_y{ static_cast<double>(canvas_space::height) /
                 dot_in_line_count };

    std::vector<triangle_space::vertex> triangle_v;
    std::vector<uint16_t>               indexes_v;

    for (double x{ step_x / 2 }; x < canvas_space::width; x += step_x)
    {
        for (double y{ step_y / 2 }; y < canvas_space::height; y += step_y)
        {
            triangle_v.push_back({ x, y, 0, 1, 0, x, y, 0 });
        }
    }
    for (size_t x{ 0 }; x < dot_in_line_count - 1; x++)
    {
        for (size_t y{ 0 }; y < dot_in_line_count - 1; y++)
        {
            uint16_t index0 =
                static_cast<uint16_t>(y * (dot_in_line_count) + x);
            uint16_t index1 = index0 + 1;
            uint16_t index2 =
                static_cast<uint16_t>(index0 + (dot_in_line_count));

            indexes_v.push_back(index0);
            indexes_v.push_back(index1);
            indexes_v.push_back(index1);

            indexes_v.push_back(index0);
            indexes_v.push_back(index2);
            indexes_v.push_back(index2);
            if (x == 0 || y == 0)
            {
                triangle_v.at(index0).f7 = 1;
            }
        }
    }
    for (size_t x{ dot_in_line_count - 1 }, y{ 0 }; y < dot_in_line_count - 1;
         y++)
    {
        uint16_t index0 = static_cast<uint16_t>(y * (dot_in_line_count) + x);
        uint16_t index1 = static_cast<uint16_t>(index0 + (dot_in_line_count));

        indexes_v.push_back(index0);
        indexes_v.push_back(index1);
        indexes_v.push_back(index1);
        triangle_v.at(index0).f7 = 1;
    }
    for (size_t x{ 0 }, y{ dot_in_line_count - 1 }; x < dot_in_line_count - 1;
         x++)
    {
        uint16_t index0 = static_cast<uint16_t>(y * (dot_in_line_count) + x);
        uint16_t index1 = index0 + 1;

        indexes_v.push_back(index0);
        indexes_v.push_back(index1);
        indexes_v.push_back(index1);
        triangle_v.at(index0).f7 = 1;
        triangle_v.at(index1).f7 = 1;
    }

    void*     pixels = image.data();
    const int depth  = sizeof(canvas_space::color) * 8;
    const int pitch  = canvas_space::width * sizeof(canvas_space::color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    interpolated_render.set_gfx_program(program01);

    double mouse_x{};
    double mouse_y{};
    double radius{ std::min(step_x, step_y) }; // 20 pixels radius

    bool continue_loop = true;

    while (continue_loop)
    {
        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
            {
                continue_loop = false;
                break;
            }
            else if (e.type == SDL_MOUSEMOTION)
            {
                mouse_x = e.motion.x;
                mouse_y = e.motion.y;
            }
            else if (e.type == SDL_MOUSEWHEEL)
            {
                radius *= e.wheel.y;
            }
        }

        interpolated_render.clear(black);
        program01.set_uniforms(
            triangle_space::uniforms{ mouse_x, mouse_y, radius });

        interpolated_render.draw_triangles(triangle_v, indexes_v);

        SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
            pixels, canvas_space::width, canvas_space::height, depth, pitch,
            rmask, gmask, bmask, amask);
        if (bitmapSurface == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_Texture* bitmapTex =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_FreeSurface(bitmapSurface);

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapTex);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
