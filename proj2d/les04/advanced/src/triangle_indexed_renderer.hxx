#pragma once
#include "triangle_renderer.hxx"

namespace triangle_space
{
class triangle_indexed_renderer : public triangle_renderer
{
public:
    triangle_indexed_renderer(canvas_space::canvas& canvas, size_t width,
                              size_t height);
    void draw_triangles(canvas_space::pixels_position& positions,
                        std::vector<uint8_t>&          indexes,
                        canvas_space::color            color);
};
} // namespace triangle_space
