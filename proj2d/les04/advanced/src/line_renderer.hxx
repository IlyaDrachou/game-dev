#pragma once
#include "../include/canvas.hxx"

namespace line_space
{
class line_renderer : canvas_space::irender
{
public:
    line_renderer(canvas_space::canvas& canvas, size_t width, size_t height);
    void clear(canvas_space::color color) override;
    void set_pixel(canvas_space::position position,
                   canvas_space::color    color) override;
    virtual canvas_space::pixels_position pixels_pos(
        canvas_space::position start, canvas_space::position end) override;
    void draw_line(canvas_space::position start, canvas_space::position end,
                   canvas_space::color color);

private:
    canvas_space::canvas& canvas;
    const size_t          width;
    const size_t          height;
};
} // namespace line_space
