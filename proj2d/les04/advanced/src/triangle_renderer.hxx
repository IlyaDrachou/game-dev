#pragma once
#include "line_renderer.hxx"

namespace triangle_space
{
class triangle_renderer : public line_space::line_renderer
{
public:
    triangle_renderer(canvas_space::canvas& canvas, size_t width,
                      size_t height);
    virtual canvas_space::pixels_position pixels_pos(canvas_space::position v0,
                                                     canvas_space::position v1,
                                                     canvas_space::position v2);
    void draw_triangles(canvas_space::pixels_position& vertexes,
                        size_t vertexes_count, canvas_space::color color);
};
} // namespace triangle_space
