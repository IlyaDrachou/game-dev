cmake_minimum_required (VERSION 3.13)
project(projects2d)

add_subdirectory(les01)
add_subdirectory(les02)
add_subdirectory(les03)
add_subdirectory(les04)
