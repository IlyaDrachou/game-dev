#include <cstdlib>
#include <iostream>
#include <string>

#include <hello_lib.hxx>

int main(int, char **) {
  auto username_env = std::getenv("USER");
  std::string username = (username_env != nullptr) ? username_env : "Anonimus";

  auto result = greeting(username.c_str());
  return (result ? EXIT_SUCCESS : EXIT_FAILURE);
}
