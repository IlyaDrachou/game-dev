#include "hello_lib.hxx"
#include <iostream>
#include <string>

bool greeting(const char *username) {
  std::cout << "Hello, " << username;
  return std::cout.good();
}
