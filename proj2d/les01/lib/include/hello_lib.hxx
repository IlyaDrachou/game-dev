#include <string>

#ifdef _WIN32
#ifdef hello_lib_dynamic_EXPORTS
#define HELLO_BIN_DYNAMIC_API __declspec(dllexport)
#else
#define HELLO_BIN_DYNAMIC_API __declspec(dllimport)
#endif
#ifndef IS_STATIC
extern "C" HELLO_BIN_DYNAMIC_API
#endif // IS_STATIC
#endif // _WIN32

    bool
    greeting(const char *username);
