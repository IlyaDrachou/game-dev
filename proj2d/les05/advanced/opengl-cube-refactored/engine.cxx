#include "engine.hxx"
#include <SDL.h>
#include <SDL_keycode.h>
#include <SDL_version.h>
#if defined(__MINGW32__) || defined(__APPLE__)
#include <SDL_opengl.h> // on windows for mingw
#include <SDL_opengl_glext.h>
#endif
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string_view>

#ifdef WIN32
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#else
#include <glm/gtc/matrix_transform.hpp>
#endif
#include <glm/glm.hpp>

#include "include/check_input.hxx"
#include "include/shaders.hxx"

namespace engn
{
class engine_impl final : public engine
{
    engine_impl() {}

public:
    friend engine* create_engine(); // For anti-pattern singlton
    std::string    initialize(std::string_view) override final;
    bool           read_input(event_type& e) override final;
    void           render_triangle(const triangle& tr) override final;
    void           swap_buffers() override final;
    void           uinitialize() override final;

private:
    SDL_Window*   window{ nullptr };
    SDL_GLContext gl_context{ nullptr };
    GLuint        program_id = 0;

    static fps_camera camera;
    float delta_time{ 0.0f }; // Time between current frame and last frame
    float last_frame{ 0.0f }; // Time of last frame
};
fps_camera engine_impl::camera =
    fps_camera({ 0.f, 0.f, 1.f }, { 0.f, 0.f, -1.f }, { 0.f, 1.f, 0.f });
engine::~engine() {}

static engine* e = nullptr;
engine*        create_engine()
{
    if (e != nullptr)
    {
        throw std::runtime_error("Engine already exists!!!");
    }
    e = new engine_impl;
    return e;
}

void destroy_engine(engine* e)
{
    if (e == nullptr)
    {
        throw std::runtime_error("Engine already destroyed!!!");
    }
    delete e;
}

std::string engine_impl::initialize(std::string_view)
{
    using namespace std;

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        const char* err_message = SDL_GetError();
        std::string err_str{ "error: failed call SDL_Init: " };
        return err_str + err_message;
    }

    // On Apple's OS X you must set the NSHighResolutionCapable Info.plist
    // property to YES, otherwise you will not receive a High DPI OpenGL canvas.
    // just read:
    // https://stackoverflow.com/questions/1596945/building-osx-app-bundle
    window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, 640, 480,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

    if (window == nullptr)
    {
        const char* err_message = SDL_GetError();
        std::string err_str{ "error: failed call SDL_CreateWindow: " };
        SDL_Quit();
        return err_str + err_message;
    }

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        std::string msg("can't create opengl context: ");
        msg += SDL_GetError();
        return msg;
    }

    auto load_result = gladLoadGLES2Loader(SDL_GL_GetProcAddress);
    if (load_result == 0)
    {
        std::string err_str{ "Can't load GLES2 functions!" };
        return err_str;
    }

    // create vertex shader
    GLuint vert_shader{};
    auto   err_string{ shaders::load_shader(vert_shader, true,
                                          "resources/vertex_shader.vsh") };
    if (!err_string.empty())
    {
        return err_string;
    }

    // create fragment shader
    GLuint fragment_shader{};
    err_string = shaders::load_shader(fragment_shader, false,
                                      "resources/fragment_shader.fsh");
    if (!err_string.empty())
    {
        return err_string;
    }

    err_string =
        shaders::setup_gl_program(program_id, vert_shader, fragment_shader);
    if (!err_string.empty())
    {
        return err_string;
    }
    // turn on rendering with just created shader program
    glUseProgram(program_id);
    ENGN_GL_CHECK()

    const float tmp{ 640 / 480 };
    camera.aspect(tmp);

    return "";
}

bool engine_impl::read_input(event_type& e)
{
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event))
    {
        check_input check;
        switch (sdl_event.type)
        {
            case SDL_QUIT:
            {
                e = event_type::QUIT;
                return true;
            }
            case SDL_KEYDOWN:
            {
                e = event_type::KEY_PRESSED;
                std::function<void(fps_camera&)> move_func;
                auto result{ check(sdl_event.key, move_func) };
                move_func(camera);
                return result;
            }
            case SDL_KEYUP:
            {
                e = event_type::KEY_RELEASED;
                std::function<void(fps_camera&)> move_func;
                auto result{ check(sdl_event.key, move_func) };
                move_func(camera);
                return result;
            }
            case SDL_MOUSEWHEEL:
            {
                e = event_type::MOUSEWHEEl;
                std::function<void(fps_camera&)> scale_func;
                auto result{ check(sdl_event.wheel, scale_func) };
                scale_func(camera);
                return result;
            }
            case SDL_MOUSEMOTION:
            {
                e = event_type::MOUSEMOTION;
                std::function<void(fps_camera&)> func;
                auto result{ check(sdl_event.motion, func) };
                func(camera);
                return result;
            }
        }
    }
    return false;
}

void engine_impl::render_triangle(const triangle& tr)
{
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), &tr.v[0]);
    ENGN_GL_CHECK()
    glEnableVertexAttribArray(0);
    ENGN_GL_CHECK()
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), &tr.v[0].r);
    ENGN_GL_CHECK()
    glEnableVertexAttribArray(1);
    ENGN_GL_CHECK()

    float currentFrame = SDL_GetTicks() * 0.001f; // seconds
    delta_time         = currentFrame - last_frame;
    last_frame         = currentFrame;

    camera.update(delta_time);
    glm::mat4 projection = camera.projection_matrix();
    glm::mat4 view       = camera.view_matrix();
    glm::mat4 model =
        glm::translate(camera.model_matrix(), glm::vec3(-1.5f, -2.2f, -2.5f));

    glm::mat4 MVP       = projection * view * model;
    GLint     matrix_id = glGetUniformLocation(program_id, "MVP");
    glUniformMatrix4fv(matrix_id, 1, GL_FALSE, &MVP[0][0]);

    glValidateProgram(program_id);
    ENGN_GL_CHECK()
    GLint validate_status = 0;
    glGetProgramiv(program_id, GL_VALIDATE_STATUS, &validate_status);
    ENGN_GL_CHECK()
    if (validate_status == GL_FALSE)
    {
        GLint infoLen = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infoLen);
        ENGN_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id, infoLen, nullptr, infoLog.data());
        ENGN_GL_CHECK()
        std::cerr << "Error linking program:\n" << infoLog.data();
        throw std::runtime_error("error");
    }

    glDrawArrays(GL_TRIANGLES, 0, 3);
    ENGN_GL_CHECK()
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void engine_impl::swap_buffers()
{
    SDL_GL_SwapWindow(window);

    glClearColor(1.0, 1.0f, 1.0f, 0.0f);
    ENGN_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ENGN_GL_CHECK()
}

void engine_impl::uinitialize()
{
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // namespace engn
