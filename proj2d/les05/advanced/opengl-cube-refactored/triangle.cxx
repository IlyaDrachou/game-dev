#include "triangle.hxx"

namespace engn
{
std::istream& operator>>(std::istream& in, vertex& v)
{
    in >> v.x;
    in >> v.y;
    in >> v.z;

    in >> v.r;
    in >> v.g;
    in >> v.b;
    return in;
}

std::istream& operator>>(std::istream& in, triangle& tr)
{
    in >> tr.v[0];
    in >> tr.v[1];
    in >> tr.v[2];
    return in;
}

} // namespace engn
