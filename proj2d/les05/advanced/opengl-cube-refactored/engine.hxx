#pragma once
#include <string_view>
#include <unordered_map>

#include "triangle.hxx"

#ifndef ENGN_DECLSPEC
#define ENGN_DECLSPEC
#endif

namespace engn
{
enum class event_type
{
    KEY_PRESSED,
    KEY_RELEASED,
    MOUSEMOTION,
    MOUSEWHEEl,
    MOUSEBUTTONDOWN,
    MOUSEBUTTONUP,
    TOUCH_FINGERDOWN,
    TOUCH_FINGERUP,
    TOUCH_FINGERMOTION,
    QUIT
};

class ENGN_DECLSPEC engine
{
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view)        = 0;
    virtual bool        read_input(event_type& e)           = 0;
    virtual void        render_triangle(const triangle& tr) = 0;
    virtual void        swap_buffers()                      = 0;
    virtual void        uinitialize()                       = 0;
};

ENGN_DECLSPEC engine* create_engine();
ENGN_DECLSPEC void    destroy_engine(engine*);
} // namespace engn
