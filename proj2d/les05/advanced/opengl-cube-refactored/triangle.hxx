#pragma once
#include <iostream>

#ifndef ENGN_DECLSPEC
#define ENGN_DECLSPEC
#endif

namespace engn
{
struct ENGN_DECLSPEC vertex
{
    float x{ 0.f };
    float y{ 0.f };
    float z{ 0.f };
    float r{ 0.f };
    float g{ 0.f };
    float b{ 0.f };
};

struct ENGN_DECLSPEC triangle
{
    triangle()
    {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

ENGN_DECLSPEC std::istream& operator>>(std::istream& in, vertex& v);
ENGN_DECLSPEC std::istream& operator>>(std::istream& in, triangle& tr);

} // namespace engn
