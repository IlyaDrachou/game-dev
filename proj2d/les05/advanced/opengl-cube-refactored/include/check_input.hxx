#pragma once
#ifndef ENGN_DECLSPEC
#define ENGN_DECLSPEC
#endif
#include <SDL.h>

#include <array>
#include <functional>
#include <map>
#include <string>

#include "engine.hxx"
#include "fps_camera.hxx"
#include "triangle.hxx"

namespace engn
{
// struct for keys handling
#pragma pack(push, 4)
struct bind
{
    bind(SDL_Scancode key, event_action action)
        : m_key(key)
        , m_action(action)
    {
    }
    SDL_Scancode m_key;
    event_action m_action;
};
#pragma pack(pop)

static std::array<bind, 8> handled_keys{
    { { SDL_SCANCODE_W, event_action::MOVE_AHEAD },
      { SDL_SCANCODE_UP, event_action::MOVE_AHEAD },
      { SDL_SCANCODE_S, event_action::MOVE_BACK },
      { SDL_SCANCODE_DOWN, event_action::MOVE_BACK },
      { SDL_SCANCODE_A, event_action::MOVE_LEFT },
      { SDL_SCANCODE_LEFT, event_action::MOVE_LEFT },
      { SDL_SCANCODE_D, event_action::MOVE_RIGHT },
      { SDL_SCANCODE_RIGHT, event_action::MOVE_RIGHT } }
};

class check_input
{
public:
    check_input() {}
    bool operator()(const SDL_KeyboardEvent&          event_key,
                    std::function<void(fps_camera&)>& move_func);
    bool operator()(const SDL_MouseMotionEvent&       motion,
                    std::function<void(fps_camera&)>& func);
    bool operator()(const SDL_MouseWheelEvent&        wheel_event,
                    std::function<void(fps_camera&)>& scale_func);
};
} // namespace engn
