#pragma once
#include <glm/glm.hpp>
#include <map>

namespace engn
{
enum class event_action
{
    MOVE_AHEAD,
    MOVE_BACK,
    MOVE_LEFT,
    MOVE_RIGHT
};

class fps_camera
{
    struct
    {
        bool move_ahead{ false };
        bool move_back{ false };
        bool move_right{ false };
        bool move_left{ false };
    } direction;

public:
    fps_camera() = default;
    fps_camera(glm::vec3 camera_pos, glm::vec3 camera_direction,
               glm::vec3 camera_up);

    void update(const float delta_time);
    void update_yaw(const float delta_yaw);
    void update_pitch(const float delta_pitch);
    void update_scale(const float delta_scale);
    void move_direction(const event_action& direction, const bool is_move);

    glm::mat4 view_matrix() const;
    glm::mat4 projection_matrix() const;
    glm::mat4 model_matrix() const;

    float aspect() const;
    void  aspect(const float value);

private:
    float fov_{ 45.f };
    float aspect_{ 1.f };
    float z_near_{ 0.1f };
    float z_far_{ 100.f };

    // camera attributes
    glm::vec3 position_;
    glm::vec3 front{ 0.f, 0.f, -1.f };
    glm::vec3 up{ 0.f, 1.f, 0.f };
    glm::vec3 right;
    glm::vec3 world_up{ 0.f, 1.f, 0.f };
    float     scale{ 1.f };
    // Euler angles
    float yaw{ 0.f };   /// in degrees
    float pitch{ 0.f }; /// in degrees
    // camera options
    float movement_speed{ 3.0f };
};
} // namespace engn
