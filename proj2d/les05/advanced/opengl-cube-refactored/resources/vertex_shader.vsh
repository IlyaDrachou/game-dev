attribute vec3 a_position;
attribute vec3 a_color;

uniform mat4 MVP;

varying vec4 v_position;
varying vec3 v_color;

void main()
{
  v_position = MVP * vec4(a_position,1);
  v_color = a_color;
  gl_Position = v_position;
}
