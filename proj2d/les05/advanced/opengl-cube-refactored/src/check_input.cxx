#include "include/check_input.hxx"
#include <functional>

bool engn::check_input::operator()(const SDL_KeyboardEvent&          event_key,
                                   std::function<void(fps_camera&)>& move_func)
{
    auto bind_it = std::find_if(
        std::begin(handled_keys), std::end(handled_keys),
        [&](const bind& b) { return b.m_key == event_key.keysym.scancode; });

    if (bind_it == end(handled_keys))
    {
        return false;
    }

    event_action& direction{ bind_it->m_action };
    move_func = [&](fps_camera& camera) {
        if (event_key.type == SDL_KEYDOWN)
            camera.move_direction(direction, true);
        else if (event_key.type == SDL_KEYUP)
            camera.move_direction(direction, false);
    };

    return true;
}

bool engn::check_input::operator()(const SDL_MouseMotionEvent&       motion,
                                   std::function<void(fps_camera&)>& func)
{
    func = [&](fps_camera& camera) {
        const float sencivity{ 0.5f };
        const float delta_yaw   = motion.xrel * sencivity;
        const float delta_pitch = -1.f * motion.yrel * sencivity;

        camera.update_yaw(delta_yaw);
        camera.update_pitch(delta_pitch);
    };
    if (motion.xrel == 0 && motion.yrel == 0)
    {
        return false;
    }
    return true;
}

bool engn::check_input::operator()(const SDL_MouseWheelEvent& wheel_event,
                                   std::function<void(fps_camera&)>& scale_func)
{
    scale_func = [&](fps_camera& camera) {
        camera.update_scale(-wheel_event.y);
    };
    if (wheel_event.y == 0)
    {
        return false;
    }
    return true;
}
