#include "include/fps_camera.hxx"
#include <algorithm>
#include <glm/gtc/matrix_transform.hpp>

namespace engn
{
fps_camera::fps_camera(glm::vec3 camera_pos, glm::vec3 camera_direction,
                       glm::vec3 camera_up)
{
    camera_direction = glm::normalize(camera_direction);
    float yaw_rad    = std::asin(camera_direction.x);
    yaw              = glm::degrees(yaw_rad);

    float pitch_rad = std::asin(camera_direction.y);
    pitch           = glm::degrees(pitch_rad);

    position_ = camera_pos;
    world_up  = glm::normalize(camera_up);

    update(0);
}

void fps_camera::update(const float delta_time)
{
    glm::vec3 front_;
    float     yaw_rad   = glm::radians(yaw);
    float     pitch_rad = glm::radians(pitch);
    front_.x            = std::sin(yaw_rad);
    front_.y            = std::sin(pitch_rad);
    front_.z            = -1.f * cos(pitch_rad) * cos(yaw_rad);

    front = glm::normalize(front_);
    right = glm::normalize(glm::cross(front, world_up));
    up    = glm::normalize(cross(right, front));

    const float velocity = movement_speed * delta_time;

    if (direction.move_ahead && !direction.move_back)
    {
        position_ += front * velocity;
    }
    if (!direction.move_ahead && direction.move_back)
    {
        position_ -= front * velocity;
    }
    if (direction.move_right && !direction.move_left)
    {
        position_ += right * velocity;
    }
    if (!direction.move_right && direction.move_left)
    {
        position_ -= right * velocity;
    }
}

void fps_camera::update_yaw(const float delta_yaw)
{
    yaw += delta_yaw;
}

void fps_camera::update_pitch(const float delta_pitch)
{
    pitch += delta_pitch;
    pitch = std::clamp(pitch, -89.f, 89.f);
}

void fps_camera::update_scale(const float delta_scale)
{
    fov_ += delta_scale;

    fov_ = std::clamp(fov_, 1.f, 45.f);
}

void fps_camera::move_direction(const event_action& direction,
                                const bool          is_move)
{

    switch (direction)
    {
        case event_action::MOVE_AHEAD:
        {
            this->direction.move_ahead = is_move;
            break;
        }
        case event_action::MOVE_BACK:
        {
            this->direction.move_back = is_move;
            break;
        }
        case event_action::MOVE_RIGHT:
        {
            this->direction.move_right = is_move;
            break;
        }
        case event_action::MOVE_LEFT:
        {
            this->direction.move_left = is_move;
            break;
        }
    }
}

glm::mat4 fps_camera::view_matrix() const
{
    return glm::lookAt(position_, position_ + front, up);
}

glm::mat4 fps_camera::projection_matrix() const
{
    return glm::perspective(glm::radians(fov_), aspect_, z_near_, z_far_);
}

glm::mat4 fps_camera::model_matrix() const
{
    return glm::scale(glm::mat4(1.0f), glm::vec3(scale));
}

float fps_camera::aspect() const
{
    return aspect_;
}

void fps_camera::aspect(const float value)
{
    aspect_ = value;
}
} // namespace engn
