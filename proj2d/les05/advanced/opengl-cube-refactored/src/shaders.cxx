#include "include/shaders.hxx"
#include <assert.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace engn
{
std::string shaders::load_shader(GLuint& shader_id, const bool is_vertex_shader,
                                 const std::string& file_name)
{
    std::stringstream serr{};
    std::string shader_type_name = is_vertex_shader ? "vertex" : "fragment";

    shader_id = is_vertex_shader ? glCreateShader(GL_VERTEX_SHADER)
                                 : glCreateShader(GL_FRAGMENT_SHADER);
    ENGN_GL_CHECK()
    std::ifstream shader_file(file_name);
    if (!shader_file.is_open())
    {
        serr << "Error: can't open shader source file(" << shader_type_name
             << ")\n";
        return serr.str();
    }
    std::string shader_source;
    shader_source.assign(std::istreambuf_iterator<char>(shader_file),
                         std::istreambuf_iterator<char>());
    auto source = shader_source.data();
    shader_file.close();
    glShaderSource(shader_id, 1, &source, nullptr);
    ENGN_GL_CHECK()

    glCompileShader(shader_id);
    ENGN_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
    ENGN_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
        ENGN_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
        ENGN_GL_CHECK()
        glDeleteShader(shader_id);
        ENGN_GL_CHECK()

        serr << "Error compiling shader( " << shader_type_name << ")\n"
             << shader_source << "\n"
             << info_chars.data();
        return serr.str();
    }

    return "";
}

std::string shaders::setup_gl_program(GLuint&       program_id,
                                      const GLuint& vertex_shader,
                                      const GLuint& fragment_shader)
{
    std::stringstream serr{};

    program_id = glCreateProgram();
    ENGN_GL_CHECK()
    if (program_id == 0)
    {
        return "Error: failed to create gl program";
    }
    // now create program and attach vertex and fragment shaders
    glAttachShader(program_id, vertex_shader);
    ENGN_GL_CHECK()
    glAttachShader(program_id, fragment_shader);
    ENGN_GL_CHECK()

    // bind attribute location
    glBindAttribLocation(program_id, 0, "a_position");
    ENGN_GL_CHECK()
    glBindAttribLocation(program_id, 1, "a_color");
    // link program after binding attribute locations
    glLinkProgram(program_id);
    ENGN_GL_CHECK()
    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &linked_status);
    ENGN_GL_CHECK()
    if (linked_status == 0)
    {
        GLint infoLen = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infoLen);
        ENGN_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id, infoLen, nullptr, infoLog.data());
        ENGN_GL_CHECK()
        serr << "Error linking program:\n" << infoLog.data();
        glDeleteProgram(program_id);
        ENGN_GL_CHECK()
        return serr.str();
    }
    return "";
}
} // namespace engn
