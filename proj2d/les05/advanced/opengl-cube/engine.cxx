#include "engine.hxx"
#include <SDL.h>
#include <SDL_keycode.h>
#include <SDL_version.h>
#if defined(__MINGW32__) || defined(__APPLE__)
#include <SDL_opengl.h> // on windows for mingw
#include <SDL_opengl_glext.h>
#else
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#endif
#include <algorithm>
#include <array>
#include <assert.h>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <unordered_map>
#include <vector>

#ifdef WIN32
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#else
#include <glm/gtc/matrix_transform.hpp>
#endif

#include <glm/glm.hpp>

// we have to load all extension GL function pointers
// dynamically from opengl library
// so first declare function pointers for all we need
static PFNGLCREATESHADERPROC            glCreateShader            = nullptr;
static PFNGLSHADERSOURCEPROC            glShaderSource            = nullptr;
static PFNGLCOMPILESHADERPROC           glCompileShader           = nullptr;
static PFNGLGETSHADERIVPROC             glGetShaderiv             = nullptr;
static PFNGLGETSHADERINFOLOGPROC        glGetShaderInfoLog        = nullptr;
static PFNGLDELETESHADERPROC            glDeleteShader            = nullptr;
static PFNGLCREATEPROGRAMPROC           glCreateProgram           = nullptr;
static PFNGLATTACHSHADERPROC            glAttachShader            = nullptr;
static PFNGLBINDATTRIBLOCATIONPROC      glBindAttribLocation      = nullptr;
static PFNGLLINKPROGRAMPROC             glLinkProgram             = nullptr;
static PFNGLGETPROGRAMIVPROC            glGetProgramiv            = nullptr;
static PFNGLGETPROGRAMINFOLOGPROC       glGetProgramInfoLog       = nullptr;
static PFNGLGETUNIFORMLOCATIONPROC      glGetUniformLocation      = nullptr;
static PFNGLUNIFORMMATRIX4FVPROC        glUniformMatrix4fv        = nullptr;
static PFNGLDELETEPROGRAMPROC           glDeleteProgram           = nullptr;
static PFNGLUSEPROGRAMPROC              glUseProgram              = nullptr;
static PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer     = nullptr;
static PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
static PFNGLVALIDATEPROGRAMPROC         glValidateProgram         = nullptr;
// RENDER_DOC//////////////////////
static PFNGLBINDBUFFERPROC      glBindBuffer      = nullptr;
static PFNGLGENBUFFERSPROC      glGenBuffers      = nullptr;
static PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
static PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
static PFNGLBUFFERDATAPROC      glBufferData      = nullptr;
// RENDER_DOC//////////////////////

template <class T>
static void load_gl_func(const char* func_name, T& result)
{
    auto gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("Can't load GL function") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define ENGN_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace engn
{
using namespace std;

// keys for event searching should be only UPPERCASE, construct: "eventName" +
// + '_' + "eventType" (ex. AHEAD_PRESSED). For QUIT only eventType.
static std::unordered_map<string, event_pair> actions{
    { "AHEAD_PRESSED", { event::AHEAD_PRESSED, "Ahead_pressed" } },
    { "AHEAD_RELEASED", { event::AHEAD_RELEASED, "Ahead_released" } },
    { "BACK_PRESSED", { event::BACK_PRESSED, "Back_pressed" } },
    { "BACK_RELEASED", { event::BACK_RELEASED, "Back_released" } },
    { "LEFT_PRESSED", { event::LEFT_PRESSED, "Left_pressed" } },
    { "LEFT_RELEASED", { event::LEFT_RELEASED, "Left_released" } },
    { "RIGHT_PRESSED", { event::RIGHT_PRESSED, "Right_pressed" } },
    { "RIGHT_RELEASED", { event::RIGHT_RELEASED, "Right_released" } },
    { "QUIT", { event::QUIT, "Quit" } }
};

// struct for keys handling
#pragma pack(push, 4)
struct bind
{
    bind(SDL_Scancode key, std::string_view action)
        : m_key(key)
        , m_action(action)
    {
    }
    SDL_Scancode     m_key;
    std::string_view m_action;
};
#pragma pack(pop)

static array<bind, 8> handled_keys{ { { SDL_SCANCODE_W, "AHEAD" },
                                      { SDL_SCANCODE_UP, "AHEAD" },
                                      { SDL_SCANCODE_S, "BACK" },
                                      { SDL_SCANCODE_DOWN, "BACK" },
                                      { SDL_SCANCODE_A, "LEFT" },
                                      { SDL_SCANCODE_LEFT, "LEFT" },
                                      { SDL_SCANCODE_D, "RIGHT" },
                                      { SDL_SCANCODE_RIGHT, "RIGHT" } } };

static bool check_input(std::string_view   event_type,
                        const SDL_Keycode& event_key, event_pair& e)
{
    std::string postfix{ event_type };
    std::transform(begin(postfix), end(postfix), begin(postfix), ::toupper);

    auto prefix = find_if(std::begin(handled_keys), std::end(handled_keys),
                          [&](const bind& b) { return b.m_key == event_key; });

    if (prefix == end(handled_keys))
    {
        return false;
    }

    e = actions.at(string(prefix->m_action) + '_' + postfix);

    return true;
}

static std::pair<std::string, GLuint> load_shaders()
{
    using namespace std;

    stringstream serr;

    // RENDER_DOC///////////////////////////////////////////
    GLuint vertex_buffer = 0;
    glGenBuffers(1, &vertex_buffer);
    ENGN_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    ENGN_GL_CHECK()
    GLuint vertex_array_object = 0;
    glGenVertexArrays(1, &vertex_array_object);
    ENGN_GL_CHECK()
    glBindVertexArray(vertex_array_object);
    ENGN_GL_CHECK()
    // RENDER_DOC///////////////////////////////////////////
    // create vertex shader

    GLuint vert_shader = glCreateShader(GL_VERTEX_SHADER);
    ENGN_GL_CHECK()
    string_view vertex_shader_src = R"(#version 330 core
                                    layout (location = 0) in vec3 a_position;
                                    layout (location = 1) in vec3 a_color;

                                    uniform mat4 MVP;

                                    out vec4 v_position;
                                    out vec3 v_color;

                                    void main()
                                    {
                                      v_position = MVP * vec4(a_position,1);
                                      v_color = a_color;
                                      gl_Position = v_position;
                                    }
                                    )";
    const char* source            = vertex_shader_src.data();
    glShaderSource(vert_shader, 1, &source, nullptr);
    ENGN_GL_CHECK()

    glCompileShader(vert_shader);
    ENGN_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(vert_shader, GL_COMPILE_STATUS, &compiled_status);
    ENGN_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(vert_shader, GL_INFO_LOG_LENGTH, &info_len);
        ENGN_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(vert_shader, info_len, nullptr, info_chars.data());
        ENGN_GL_CHECK()
        glDeleteShader(vert_shader);
        ENGN_GL_CHECK()

        std::string shader_type_name = "vertex";
        serr << "Error compiling shader(vertex)\n"
             << vertex_shader_src << "\n"
             << info_chars.data();
        return std::pair(serr.str(), 0);
    }

    // create fragment shader

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    ENGN_GL_CHECK()
    string_view fragment_shader_src = R"(#version 330 core
                                      in vec4 v_position;
                                      in vec3 v_color;
                                      out vec4 FragColor;
                                      void main()
                                      {
                                          FragColor = vec4(v_color, 1.0);
                                          /*
                                          if (v_position.z >= 0.0)
                                          {
                                            float light_green = 0.5 + v_position.z / 2.0;
                                            FragColor = vec4(0.0, light_green, 0.0, 1.0);
                                          } else
                                          {
                                            float dark_green = 0.5 - (v_position.z / -2.0);
                                            FragColor = vec4(0.0, dark_green, 0.0, 1.0);
                                          }
                                          */
                                      }
                                      )";
    source                          = fragment_shader_src.data();
    glShaderSource(fragment_shader, 1, &source, nullptr);
    ENGN_GL_CHECK()

    glCompileShader(fragment_shader);
    ENGN_GL_CHECK()

    compiled_status = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    ENGN_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
        ENGN_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(fragment_shader, info_len, nullptr,
                           info_chars.data());
        ENGN_GL_CHECK()
        glDeleteShader(fragment_shader);
        ENGN_GL_CHECK()

        serr << "Error compiling shader(fragment)\n"
             << vertex_shader_src << "\n"
             << info_chars.data();
        return std::pair(serr.str(), 0);
    }

    auto program_id = glCreateProgram();
    ENGN_GL_CHECK()
    if (0 == program_id)
    {
        serr << "failed to create gl program";
        return std::pair(serr.str(), 0);
    }

    glAttachShader(program_id, vert_shader);
    ENGN_GL_CHECK()
    glAttachShader(program_id, fragment_shader);
    ENGN_GL_CHECK()

    return std::pair("", program_id);
}

class engine_impl final : public engine
{
    engine_impl() {}

public:
    friend engine* create_engine(); // For anti-pattern singlton
    std::string    initialize(std::string_view) override final;
    bool           read_input(event_pair& e) override final;
    void           render_triangle(const triangle& tr) override final;
    void           swap_buffers() override final;
    void           uinitialize() override final;

private:
    SDL_Window*   window{ nullptr };
    SDL_GLContext gl_context{ nullptr };
    GLuint        program_id = 0;

    // tmp
    float scale{ 1.0f };
    float offset_x{ 1.0f };
    float offset_y{ 1.0f };
};
engine::~engine() {}

static engine* e = nullptr;
engine*        create_engine()
{
    if (e != nullptr)
    {
        throw runtime_error("Engine already exists!!!");
    }
    e = new engine_impl;
    return e;
}

void destroy_engine(engine* e)
{
    if (e == nullptr)
    {
        throw runtime_error("Engine already destroyed!!!");
    }
    delete e;
}

std::istream& operator>>(std::istream& in, vertex& v)
{
    in >> v.x;
    in >> v.y;
    in >> v.z;

    in >> v.r;
    in >> v.g;
    in >> v.b;
    return in;
}

std::istream& operator>>(std::istream& in, triangle& tr)
{
    in >> tr.v[0];
    in >> tr.v[1];
    in >> tr.v[2];
    return in;
}

std::string engine_impl::initialize(std::string_view)
{
    using namespace std;

    stringstream serr;

    SDL_version linked = { 0, 0, 0 };

    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        serr << "warning: SDL2 compiled and linked version mismatch: " << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        const char* err_message = SDL_GetError();
        serr << "error: failed call SDL_Init: " << err_message << endl;
        return serr.str();
    }

    // On Apple's OS X you must set the NSHighResolutionCapable Info.plist
    // property to YES, otherwise you will not receive a High DPI OpenGL canvas.
    // just read:
    // https://stackoverflow.com/questions/1596945/building-osx-app-bundle
    window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, 640, 480,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

    if (window == nullptr)
    {
        const char* err_message = SDL_GetError();
        serr << "error: failed call SDL_CreateWindow: " << err_message << endl;
        SDL_Quit();
        return serr.str();
    }

    // RENDER_DOC//////// next code needed for RenderDoc to work
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    // RENDER_DOC////////////////////////////////////////////////

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        std::string msg("can't create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    int gl_major_ver = 0;
    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);
    int gl_minor_ver = 0;
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    if (gl_major_ver <= 2 && gl_minor_ver < 1)
    {
        serr << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need opengl version at least: 2.1\n"
             << std::flush;
        return serr.str();
    }
    try
    {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glGetUniformLocation", glGetUniformLocation);
        load_gl_func("glUniformMatrix4fv", glUniformMatrix4fv);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
        load_gl_func("glBindBuffer", glBindBuffer);           // for RENDER_DOC
        load_gl_func("glGenBuffers", glGenBuffers);           // for RENDER_DOC
        load_gl_func("glGenVertexArrays", glGenVertexArrays); // for RENDER_DOC
        load_gl_func("glBindVertexArray", glBindVertexArray); // for RENDER_DOC
        load_gl_func("glBufferData", glBufferData);           // for RENDER_DOC
    }
    catch (std::exception& ex)
    {
        return ex.what();
    }

    // now create program and attach vertex and fragment shaders
    auto [err_str, program_id_] = load_shaders();
    if (err_str != "")
    {
        return err_str;
    }
    program_id = program_id_;

    // bind attribute location
    glBindAttribLocation(program_id, 0, "a_position");
    ENGN_GL_CHECK()
    // link program after binding attribute locations
    glLinkProgram(program_id);
    ENGN_GL_CHECK()
    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &linked_status);
    ENGN_GL_CHECK()
    if (linked_status == 0)
    {
        GLint infoLen = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infoLen);
        ENGN_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id, infoLen, nullptr, infoLog.data());
        ENGN_GL_CHECK()
        serr << "Error linking program:\n" << infoLog.data();
        glDeleteProgram(program_id);
        ENGN_GL_CHECK()
        return serr.str();
    }

    // turn on rendering with just created shader program
    glUseProgram(program_id);
    ENGN_GL_CHECK()

    glEnable(GL_DEPTH_TEST);

    return "";
}

bool engine_impl::read_input(event_pair& e)
{
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event))
    {
        switch (sdl_event.type)
        {
            case SDL_QUIT:
                e = actions.at("QUIT");
                return true;
            case SDL_KEYDOWN:
                return check_input("PRESSED", sdl_event.key.keysym.scancode, e);
            case SDL_KEYUP:
                return check_input("RELEASED", sdl_event.key.keysym.scancode,
                                   e);
            case SDL_MOUSEMOTION:
                if (sdl_event.motion.xrel != 0)
                {
                    if (sdl_event.motion.xrel < 0)
                    {
                        offset_x -= 1.0f;
                    }
                    else if (sdl_event.motion.xrel > 0)
                    {
                        offset_x += 1.0f;
                    }
                }
                else if (sdl_event.motion.yrel != 0)
                {
                    if (sdl_event.motion.yrel < 0)
                    {
                        offset_y -= 1.0f;
                    }
                    else if (sdl_event.motion.yrel > 0)
                    {
                        offset_y += 1.0f;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            case SDL_MOUSEWHEEL:
                if (sdl_event.wheel.y > 0)
                {
                    scale += 0.1f;
                }
                else if (scale > 0.0f)
                {
                    scale -= 0.1f;
                }
                std::cout << scale << std::endl;
                return true;
        }
    }
    return false;
}

void engine_impl::render_triangle(const triangle& tr)
{
    glBufferData(GL_ARRAY_BUFFER, sizeof(tr), &tr, GL_STATIC_DRAW);
    ENGN_GL_CHECK()
    glEnableVertexAttribArray(0);

    GLintptr position_attr_offset = 0;

    ENGN_GL_CHECK()
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void*>(position_attr_offset));
    ENGN_GL_CHECK()
    glEnableVertexAttribArray(1);
    ENGN_GL_CHECK()

    GLintptr color_attr_offset = sizeof(float) * 3;

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void*>(color_attr_offset));

    glm::mat4 projection =
        glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
    glm::mat4 view =
        glm::lookAt(glm::vec3(1, 1, 1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
    view =
        glm::rotate(view, glm::radians(offset_x), glm::vec3(0.0f, 1.0f, 0.0f));
    view =
        glm::rotate(view, glm::radians(offset_y), glm::vec3(1.0f, 0.0f, 0.0f));
    glm::mat4 model = glm::scale(glm::mat4(1.0f), glm::vec3(scale));

    glm::mat4 MVP       = projection * view * model;
    GLint     matrix_id = glGetUniformLocation(program_id, "MVP");
    glUniformMatrix4fv(matrix_id, 1, GL_FALSE, &MVP[0][0]);

    ENGN_GL_CHECK()
    glValidateProgram(program_id);
    ENGN_GL_CHECK()
    // Check the validate status
    GLint validate_status = 0;
    glGetProgramiv(program_id, GL_VALIDATE_STATUS, &validate_status);
    ENGN_GL_CHECK()
    if (validate_status == GL_FALSE)
    {
        GLint infoLen = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infoLen);
        ENGN_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id, infoLen, nullptr, infoLog.data());
        ENGN_GL_CHECK()
        std::cerr << "Error linking program:\n" << infoLog.data();
        throw std::runtime_error("error");
    }
    glDrawArrays(GL_TRIANGLES, 0, 3);
    ENGN_GL_CHECK()

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void engine_impl::swap_buffers()
{
    SDL_GL_SwapWindow(window);

    glClearColor(1.0, 1.0f, 1.0f, 0.0f);
    ENGN_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ENGN_GL_CHECK()
}

void engine_impl::uinitialize()
{
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // namespace engn
