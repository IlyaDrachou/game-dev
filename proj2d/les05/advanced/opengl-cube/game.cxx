#include "engine.hxx"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

int main(int, char**)
{
    using namespace std;
    unique_ptr<engn::engine, void (*)(engn::engine*)> engine(
        engn::create_engine(), engn::destroy_engine);

    try
    {
        const std::string error = engine->initialize("");
        if (!error.empty())
        {
            std::cerr << error << std::endl;
            return EXIT_FAILURE;
        }

        bool continue_loop{ true };
        while (continue_loop)
        {
            engn::event_pair event_pair;
            while (engine->read_input(event_pair))
            {
                cout << event_pair.second << endl;
                if (event_pair.first == engn::event::QUIT)
                {
                    continue_loop = false;
                }
            }
            engine->swap_buffers();
            std::ifstream file("vertexes.txt");
            if (!file.is_open())
            {
                throw runtime_error("Can't open input file (vertexes)");
            }
            engn::triangle triangle;
            for (int i{}; i < 12; i++)
            {
                file >> triangle;
                engine->render_triangle(triangle);
            }
            file.close();
        }

        engine->uinitialize();
    }
    catch (runtime_error& error)
    {
        cout << error.what() << endl;
        return EXIT_FAILURE;
    }
    catch (out_of_range& error)
    {
        cout << error.what() << endl;
        cout << "Hint: check that all actions created in handled keys are "
                "exist (and action_keys too)!"
             << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
