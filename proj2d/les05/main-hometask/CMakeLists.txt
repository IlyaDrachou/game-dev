cmake_minimum_required (VERSION 3.13)
project(les05)

add_subdirectory(opengl-minimal) 
add_subdirectory(opengl-renderdoc-check)
