#pragma once
#include <string_view>
#include <unordered_map>

#ifndef ENGN_DECLSPEC
#define ENGN_DECLSPEC
#endif

namespace engn
{
enum class event
{
    AHEAD_PRESSED,
    AHEAD_RELEASED,
    BACK_PRESSED,
    BACK_RELEASED,
    LEFT_PRESSED,
    LEFT_RELEASED,
    RIGHT_PRESSED,
    RIGHT_RELEASED,
    QUIT
};

typedef std::pair<engn::event, std::string_view> event_reflection_pair;

struct ENGN_DECLSPEC vertex
{
    float x{ 0.f };
    float y{ 0.f };
    float z{ 0.f };
    float r{ 0.f };
    float g{ 0.f };
    float b{ 0.f };
};

struct ENGN_DECLSPEC triangle
{
    triangle()
    {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

ENGN_DECLSPEC std::istream& operator>>(std::istream& in, vertex& v);
ENGN_DECLSPEC std::istream& operator>>(std::istream& in, triangle& tr);

class ENGN_DECLSPEC engine
{
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view)         = 0;
    virtual bool        read_input(event_reflection_pair& e) = 0;
    virtual void        render_triangle(const triangle& tr)  = 0;
    virtual void        swap_buffers()                       = 0;
    virtual void        uinitialize()                        = 0;
};

ENGN_DECLSPEC engine* create_engine();
ENGN_DECLSPEC void    destroy_engine(engine*);
} // namespace engn
