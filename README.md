# Game-Dev (Courses)

[![Crazy-Dog](https://assets.gitlab-static.net/uploads/-/system/project/avatar/14925359/dog.png?width=64)](https://gitlab.com/IlyaDrachou/game-dev)

Build Platform   | Status (tests only)
---------------- | ----------------------
MSVC 2017 x64    | [![MSVC 2017 Build](https://ci.appveyor.com/api/projects/status/gitlab/IlyaDrachou/game-dev)](https://gitlab.com/IlyaDrachou/game-dev/commits/master)
Linux x64        | [![Linux x64](https://gitlab.com/IlyaDrachou/game-dev/badges/master/build.svg)](https://gitlab.com/IlyaDrachou/game-dev/commits/master)

## This project constains hometasks of GameDev courses.

#### Example of runtime soft render:
![](gif/soft_render.gif)

#### Description will be added on subsequent commits.
